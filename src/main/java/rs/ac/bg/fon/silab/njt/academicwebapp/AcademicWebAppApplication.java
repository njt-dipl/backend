package rs.ac.bg.fon.silab.njt.academicwebapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import rs.ac.bg.fon.silab.njt.academicwebapp.repository.UserProfileRepository;

@SpringBootApplication
@EnableJpaRepositories(basePackageClasses = UserProfileRepository.class)
public class AcademicWebAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(AcademicWebAppApplication.class, args);
    }

}

package rs.ac.bg.fon.silab.njt.academicwebapp.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AuthResponseDTO {

    private UserProfileBasicDTO userProfileBasicDTO;

    /**
     * Message for successfully authenticated user profile
     */
    private static String message = "Authentication successful.";

    /**
     * String representation of JSON Web token
     */
    private String token;
}

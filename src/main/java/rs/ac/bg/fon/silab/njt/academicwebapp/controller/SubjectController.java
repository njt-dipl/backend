/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javassist.NotFoundException;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rs.ac.bg.fon.silab.njt.academicwebapp.dto.SubjectDTO;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.Faculty;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.Subject;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.SubjectType;
import rs.ac.bg.fon.silab.njt.academicwebapp.sevice.FacultyService;
import rs.ac.bg.fon.silab.njt.academicwebapp.sevice.SubjectService;
import rs.ac.bg.fon.silab.njt.academicwebapp.sevice.SubjectTypeService;

/**
 *
 * @author Anja
 */
@RestController
@RequestMapping("/subject")
@CrossOrigin("http://localhost:4200")
public class SubjectController {

    @Autowired
    private SubjectService subjectService;
    @Autowired
    private FacultyService facultyService;
    @Autowired
    private SubjectTypeService subjectTypeService;

    @GetMapping("/getAll")
    public ResponseEntity<List<SubjectDTO>> getAll() {
        List<SubjectDTO> subjects = new ArrayList<>();
        subjectService.findAll().stream()
                .map(SubjectDTO::new)
                .forEachOrdered(subjects::add);

        for (SubjectDTO subject : subjects) {
            System.out.println(subject);
        }
        return new ResponseEntity<>(subjects, HttpStatus.OK);
    }

    @GetMapping("/getAll/{facultyId}")
    public ResponseEntity<List<SubjectDTO>> getAllForFaculty(
            @PathVariable("facultyId") Long id) {
        List<SubjectDTO> subjects = new ArrayList<>();
        subjectService.findAllForFaculty(id).stream()
                .map(SubjectDTO::new)
                .forEachOrdered(subjects::add);

        for (SubjectDTO subject : subjects) {
            System.out.println(subject);
        }
        return new ResponseEntity<>(subjects, HttpStatus.OK);
    }

    @GetMapping("/getAll/{facultyId}/{id}")
    public ResponseEntity<SubjectDTO> findByID(
            @PathVariable("id") Long id) {
        Subject subject = subjectService.findByID(id);
        SubjectDTO subjectDTO = new SubjectDTO(subject);
        return new ResponseEntity<>(subjectDTO, HttpStatus.OK);
    }

    @PostMapping("/save")
    public ResponseEntity<SubjectDTO> createSubject(
            @Valid @RequestBody SubjectDTO dto) {
        
        Subject subject = dto.convertToEntity();
        SubjectType subjectType = subjectTypeService.findByID(dto.getIdSubjectType());
        subject.setIdSubjectType(subjectType);
        Faculty faculty = facultyService.findByID(dto.getFacultyId());
        subject.setIdFaculty(faculty);

        subject = subjectService.create(subject);
        return new ResponseEntity<>(new SubjectDTO(subject), HttpStatus.OK);
    }

    @PutMapping("/getAll/{facultyId}/{id}")
    public ResponseEntity<SubjectDTO> updateSubject(
            @Valid @RequestBody SubjectDTO dto) throws NotFoundException {
        Subject subject = this.subjectService.findByID(dto.getIdSubject());
        subject.setName(dto.getName());
        subject.setShortenedName(dto.getShortenedName());
        SubjectType subjectType = subjectTypeService.findByID(dto.getIdSubjectType());
        subject.setIdSubjectType(subjectType);
        subject.setEspb(dto.getEspb());

        Subject updatedSubject = this.subjectService.update(subject);
        return new ResponseEntity<>(new SubjectDTO(updatedSubject), HttpStatus.OK);
    }

    @DeleteMapping("getAll/{id}")
    public ResponseEntity<Void> delete(@PathVariable(value = "id") Long id) {
        Subject subject = subjectService.findByID(id);
        subjectService.delete(subject.getIdSubject());
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    //getAll/${idType}
    
    @GetMapping("/getAll/type/{idType}")
    public ResponseEntity<List<SubjectDTO>> getAllForSubjectType(
            @PathVariable("idType") Long idSubjectType) {
        List<SubjectDTO> subjects = new ArrayList<>();
        subjectService.findAllForSubjectType(idSubjectType).stream()
                .map(SubjectDTO::new)
                .forEachOrdered(subjects::add);
        return new ResponseEntity<>(subjects, HttpStatus.OK);
    }
    
}

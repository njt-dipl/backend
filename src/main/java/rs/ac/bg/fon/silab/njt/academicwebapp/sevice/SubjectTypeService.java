/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.sevice;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.bg.fon.silab.njt.academicwebapp.exception.UnprocessableEntryException;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.SubjectType;
import rs.ac.bg.fon.silab.njt.academicwebapp.repository.SubjectTypeRepository;

/**
 *
 * @author Anja
 */
@Service
public class SubjectTypeService {

    @Autowired
    private SubjectTypeRepository subjectTypeRepository;

    public List<SubjectType> findAll() {
        return subjectTypeRepository.findAll();
    }
    
    public SubjectType findByID(Long id){
        return subjectTypeRepository.findById(id).orElseThrow(UnprocessableEntryException::new);
    }
}

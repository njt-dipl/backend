/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Anja
 */
@Entity
@Table(name = "faculty_grouping")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FacultyGrouping.findAll", query = "SELECT f FROM FacultyGrouping f")
    , @NamedQuery(name = "FacultyGrouping.findByIdGrouping", query = "SELECT f FROM FacultyGrouping f WHERE f.idGrouping = :idGrouping")
    , @NamedQuery(name = "FacultyGrouping.findByName", query = "SELECT f FROM FacultyGrouping f WHERE f.name = :name")})
public class FacultyGrouping implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_grouping")
    private Long idGrouping;
    @Size(max = 255)
    @Column(name = "name")
    private String name;
    @OneToMany(mappedBy = "idGrouping")
    private List<Faculty> facultyList;

    public FacultyGrouping() {
    }

    public FacultyGrouping(Long idGrouping) {
        this.idGrouping = idGrouping;
    }

    public Long getIdGrouping() {
        return idGrouping;
    }

    public void setIdGrouping(Long idGrouping) {
        this.idGrouping = idGrouping;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public List<Faculty> getFacultyList() {
        return facultyList;
    }

    public void setFacultyList(List<Faculty> facultyList) {
        this.facultyList = facultyList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idGrouping != null ? idGrouping.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FacultyGrouping)) {
            return false;
        }
        FacultyGrouping other = (FacultyGrouping) object;
        if ((this.idGrouping == null && other.idGrouping != null) || (this.idGrouping != null && !this.idGrouping.equals(other.idGrouping))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "rs.ac.bg.fon.silab.njt.academicwebapp.model.FacultyGrouping[ idGrouping=" + idGrouping + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rs.ac.bg.fon.silab.njt.academicwebapp.dto.StudyModuleDTO;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.StudyModule;
import rs.ac.bg.fon.silab.njt.academicwebapp.sevice.StudyModuleService;

/**
 *
 * @author Korisnik
 */
@RestController
@RequestMapping("/studyModule")
@CrossOrigin("http://localhost:4200")
public class StudyModuleController {

    @Autowired
    private StudyModuleService studyModuleService;

    @GetMapping("/getByStudyProgram/{idStudyProgram}")
    public ResponseEntity<List<StudyModuleDTO>> getByStudyProgram(@PathVariable("idStudyProgram") Long idStudyProgram) {
        List<StudyModuleDTO> studyModules = new ArrayList<>();
        studyModuleService.findByStudyProgram(idStudyProgram).stream()
                .map(StudyModuleDTO::new)
                .forEachOrdered(studyModules::add);

        return new ResponseEntity<>(studyModules, HttpStatus.OK);
    }

    @GetMapping("/getAll")
    public ResponseEntity<List<StudyModuleDTO>> getAll() {
        List<StudyModuleDTO> studyModules = new ArrayList<>();
        studyModuleService.findAll().stream()
                .map(StudyModuleDTO::new)
            .forEachOrdered(studyModules::add);

        return new ResponseEntity<>(studyModules, HttpStatus.OK);
    }
    
    @GetMapping("/{id}")
    public ResponseEntity<StudyModuleDTO> findByID(
            @PathVariable("id") Long id) {
        StudyModule studyModule = studyModuleService.findByID(id);
        StudyModuleDTO studyModuleDto = new StudyModuleDTO(studyModule);
        return new ResponseEntity<>(studyModuleDto, HttpStatus.OK);
    }
    
    
    ///studyProgram/${idStudyProgram}
    
    @GetMapping("/studyProgram/{idStudyProgram}")
    public ResponseEntity<StudyModuleDTO> findStudyProgramInModulByID(
            @PathVariable("idStudyProgram") Long idStudyProgram) {
        StudyModule studyModule = studyModuleService.findStudyProgramInModulByID(idStudyProgram);
        StudyModuleDTO studyModuleDto = new StudyModuleDTO(studyModule);
        return new ResponseEntity<>(studyModuleDto, HttpStatus.OK);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rs.ac.bg.fon.silab.njt.academicwebapp.dto.TeachingFormDTO;
import rs.ac.bg.fon.silab.njt.academicwebapp.sevice.TeachingFormService;

/**
 *
 * @author Anja
 */
@RestController
@RequestMapping("/teachingForm")
@CrossOrigin("http://localhost:4200")
public class TeachingFormController {
    
    @Autowired
    private TeachingFormService teachingFormService;
    
    @GetMapping("/getAll")
    public ResponseEntity<List<TeachingFormDTO>> getAll() {
        List<TeachingFormDTO> tforms = new ArrayList<>();
        teachingFormService.findAll().stream()
                .map(TeachingFormDTO::new)
                .forEachOrdered(tforms::add);

        return new ResponseEntity<>(tforms, HttpStatus.OK);
    }
    
}

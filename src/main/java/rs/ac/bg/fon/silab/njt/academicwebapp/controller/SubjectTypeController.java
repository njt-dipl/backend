/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rs.ac.bg.fon.silab.njt.academicwebapp.dto.SubjectTypeDTO;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.SubjectType;
import rs.ac.bg.fon.silab.njt.academicwebapp.sevice.SubjectTypeService;

/**
 *
 * @author Anja
 */
@RestController
@RequestMapping("/subjectType")
@CrossOrigin("http://localhost:4200")
public class SubjectTypeController {

    @Autowired
    private SubjectTypeService subjectTypeService;

    @GetMapping("/getAll")
    public ResponseEntity<List<SubjectTypeDTO>> getAllSubjectTypes() { 
        List<SubjectTypeDTO> subjectTypes = new ArrayList<>();
        subjectTypeService.findAll().stream()
                .map(SubjectTypeDTO::new)
                .forEachOrdered(subjectTypes::add);
        return new ResponseEntity<>(subjectTypes, HttpStatus.OK);
    }
}

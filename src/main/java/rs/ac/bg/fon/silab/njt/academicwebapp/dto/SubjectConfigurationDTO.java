/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.SemesterConfiguration;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.SubjectConfiguration;
//import rs.ac.bg.fon.silab.njt.academicwebapp.model.SubjectConfiguration;
//import rs.ac.bg.fon.silab.njt.academicwebapp.model.SubjectConfigurationPK;

/**
 *
 * @author Anja
 */
@Getter
@Setter
@NoArgsConstructor
public class SubjectConfigurationDTO {

    
    private Long idSubjectConfiguration;
    private Long idSemesterConfiguration;
    private Long idSubject;
//     private int semester;
//    private int position;
//    private boolean status;  
//    private Long idStudyProgram;
//    private Long idStudyModule;
    
    
    
    
      public SubjectConfigurationDTO(SubjectConfiguration subjectConfiguration) {
        this.idSubjectConfiguration = subjectConfiguration.getIdSubjectConfiguration();
        this.idSemesterConfiguration = subjectConfiguration.getIdSemesterConfiguration().getIdSemesterConfiguration();
        this.idSubject = subjectConfiguration.getIdSubject().getIdSubject();
//        this.semester = subjectConfiguration.getSemester();
//        this.position = subjectConfiguration.getPosition();
//        this.status = subjectConfiguration.getStatus();

//        this.idStudyProgram = subjectConfiguration.getSubjectConfigurationPK().getIdStudyProgram();
//        this.idStudyModule = subjectConfiguration.getSubjectConfigurationPK().getIdStudyModule();
//        
//        this.idSubjectType = subjectConfiguration.getSubject().getIdSubjectType().getIdSubjectType();
//        this.espb = subjectConfiguration.getSubject().getEspb();
    }
      
      
      
    public SubjectConfiguration convertToEntity() {
        SubjectConfiguration subjectConfiguration = new SubjectConfiguration();
        subjectConfiguration.setIdSubjectConfiguration(this.idSubjectConfiguration);
        return subjectConfiguration;
    }
    
   
//    private Long idSubjectType;
//    private int espb;
//    private String mandatory;
//
//    public SubjectConfigurationDTO(SemesterConfiguration semesterConfiguration) {
//        
//        this.idSemesterConfiguration = semesterConfiguration.getIdSemesterConfiguration();
//        this.idStudyProgram = semesterConfiguration.getIdStudyProgram().getIdStudyProgram();
//        this.idStudyModule = semesterConfiguration.getIdStudyModule().getIdStudyModule();
//        this.semester = semesterConfiguration.getSemester();
//        this.espb = semesterConfiguration.getEspb();
//        this.position = semesterConfiguration.getPosition();
//        this.status = semesterConfiguration.getStatus();
//        this.idSubjectType = semesterConfiguration.getIdSubjectType().getIdSubjectType();
//        this.mandatory = semesterConfiguration.getMandatory();
//        this.idSubject = semesterConfiguration.getIdSubject().getIdSubject();
//    }
//
//    public SemesterConfiguration convertToEntity() {
//        SemesterConfiguration semesterConfiguration = new SemesterConfiguration();
//        semesterConfiguration.setIdSemesterConfiguration(this.idSemesterConfiguration);
////        semesterConfiguration.setIdStudyProgram(this.idStudyProgram);
////        semesterConfiguration.setIdStudyModule(this.idStudyModule);
//        semesterConfiguration.setSemester(this.semester);
//        semesterConfiguration.setEspb(this.espb);
//        semesterConfiguration.setPosition(this.position);
//        semesterConfiguration.setStatus(this.status);
////        semesterConfiguration.setIdSubjectType(this.idSubjectType);
//        semesterConfiguration.setMandatory(this.mandatory);
////        semesterConfiguration.setIdSubjectType(this.idSubject);
//                
//        return semesterConfiguration;
//        
//    }

    @Override
    public String toString() {
        return "SubjectConfigurationDTO{" + "idSubjectConfiguration=" + idSubjectConfiguration + ", idSemesterConfiguration=" + idSemesterConfiguration + ", idSubject=" + idSubject + '}';
    }

  
    
    
}

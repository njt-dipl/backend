/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.TeachingForm;

/**
 *
 * @author Anja
 */
@Getter
@Setter
@NoArgsConstructor
public class TeachingFormDTO {
    
    private Long idTeachingForm;
    private String name;
    private String shortenedName;

    public TeachingFormDTO(TeachingForm tf) {
        this.idTeachingForm = tf.getIdTeachingForm();
        this.name = tf.getName();
        this.shortenedName = tf.getShortenedName();
    }
    
    public TeachingForm convertToEntity() {
        TeachingForm tf = new TeachingForm();
        tf.setIdTeachingForm(this.idTeachingForm);
        tf.setName(this.name);
        tf.setShortenedName(this.shortenedName);
        return tf;
    }
    
}

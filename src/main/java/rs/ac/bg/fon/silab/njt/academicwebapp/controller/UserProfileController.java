/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.controller;

import java.util.HashSet;
import javax.mail.MessagingException;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.ITemplateEngine;
import rs.ac.bg.fon.silab.njt.academicwebapp.dto.SuccessResponse;
import rs.ac.bg.fon.silab.njt.academicwebapp.dto.UserProfileDTO;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.UserProfile;
import rs.ac.bg.fon.silab.njt.academicwebapp.sevice.UserProfileService;

/**
 *
 * @author Korisnik
 */
@RestController
@CrossOrigin("http://localhost:4200")
public class UserProfileController {

    @Autowired
    private final UserProfileService userProfileService;
    private final ITemplateEngine springTemplateEngine;
    private final PasswordEncoder passwordEncoder;

    public UserProfileController(UserProfileService userProfileService, ITemplateEngine springTemplateEngine, PasswordEncoder passwordEncoder) {
        this.userProfileService = userProfileService;
        this.springTemplateEngine = springTemplateEngine;
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping(path = "/save")
    public ResponseEntity<UserProfileDTO> handleCreate(@Valid @RequestBody UserProfileDTO dto) throws MessagingException, Exception {
        dto.setPassword(passwordEncoder.encode(dto.getPassword()));

        UserProfile userProfile = dto.convertToEntity();
        userProfile = userProfileService.create(userProfile);
        return new ResponseEntity<>(new UserProfileDTO(userProfile), HttpStatus.OK);
    }

    @GetMapping("/verify/{code}")
    public ResponseEntity<SuccessResponse> handleVerify(@PathVariable("code") String verificationCode) {
        userProfileService.verifyUserProfile(verificationCode);
        return new ResponseEntity(userProfileService.successResponse(), HttpStatus.OK);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.sevice;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.enumeration.MandatoryEnum;

/**
 *
 * @author Anja
 */
@Service
public class MandatoryService {

    public List<String> findAll() {
        List<String> list = new ArrayList<>();
        for (MandatoryEnum value : MandatoryEnum.values()) {
            list.add(value.toString());
        }
        return list;
    }

}

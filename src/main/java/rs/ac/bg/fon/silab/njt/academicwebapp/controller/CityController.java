/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rs.ac.bg.fon.silab.njt.academicwebapp.sevice.CityService;
import rs.ac.bg.fon.silab.njt.academicwebapp.dto.CityDTO;
/**
 *
 * @author Anja
 */
@RestController
@RequestMapping("/city")
@CrossOrigin("http://localhost:4200")
public class CityController {
    
    @Autowired
    private CityService cityService;
    
    
    @GetMapping("/getAll")
    public ResponseEntity<List<CityDTO>> getAll() {
        List<CityDTO> cities = new ArrayList<>();
        cityService.findAll().stream()
                .map(CityDTO::new)
            .forEachOrdered(cities::add);

        return new ResponseEntity<>(cities, HttpStatus.OK);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.dto;

import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.Faculty;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.FacultyGrouping;

/**
 *
 * @author Anja
 */
@Getter
@Setter
@NoArgsConstructor
public class FacultyGroupingDTO {

    private Long idGrouping;

//    @NotBlank(message = "name cannot be blank")
    private String name;

//    @NotBlank(message = "faculties cannot be blank")
    private List<FacultyDTO> facultyList;

    public FacultyGroupingDTO(FacultyGrouping facultyGrouping) {
        this.idGrouping = facultyGrouping.getIdGrouping();
        this.name = facultyGrouping.getName();
        this.facultyList = new ArrayList<>();
        facultyGrouping.getFacultyList().stream()
                .map(FacultyDTO::new)
                .forEach(this.facultyList::add);
    }

    public FacultyGrouping convertToEntity(){
        FacultyGrouping facultyGrouping = new FacultyGrouping();
        facultyGrouping.setIdGrouping(this.idGrouping);
        facultyGrouping.setName(this.name);
        facultyGrouping.setFacultyList(new ArrayList<>());
        this.facultyList.stream().map(FacultyDTO::convertToEntity)
                .forEach(facultyGrouping.getFacultyList()::add);
        
        return facultyGrouping;
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.sevice;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.bg.fon.silab.njt.academicwebapp.exception.UnprocessableEntryException;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.StudyProgram;
import rs.ac.bg.fon.silab.njt.academicwebapp.repository.StudyProgramRepository;

/**
 *
 * @author Korisnik
 */
@Service
public class StudyProgramService {

    @Autowired
    private StudyProgramRepository studyProgramRepository;

    public List<StudyProgram> findByFacultyId(Long facultyId) {
        return studyProgramRepository.findByFacultyId(facultyId);
    }

    public StudyProgram findByID(Long id){
        return studyProgramRepository.findById(id).orElseThrow(UnprocessableEntryException::new);
    }
}

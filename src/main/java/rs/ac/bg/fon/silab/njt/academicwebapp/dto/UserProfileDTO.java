/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.UserProfile;

/**
 *
 * @author Korisnik
 */
@Getter
@Setter
@NoArgsConstructor
public class UserProfileDTO {

    private Long id;

    @NotBlank(message = "first name cannot be blank")
    private String firstname;

    @NotBlank(message = "last name cannot be blank")
    private String lastname;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @NotNull(message = "creation date cannot be null")
    private Date creationDate;
    @NotBlank(message = "username cannot be blank")
    private String username;

    @NotBlank(message = "email cannot be blank")
    private String email;

    @NotBlank(message = "password cannot be blank")
    private String password;

    public UserProfileDTO(UserProfile userProfile) {
        this.id = userProfile.getId();
        this.firstname = userProfile.getFirstname();
        this.lastname = userProfile.getLastname();
        this.creationDate = userProfile.getCreationDate();
        this.email = userProfile.getEmail();
        this.username = userProfile.getUsername();
        this.password = userProfile.getPassword();

    }

    public UserProfile convertToEntity() {
        UserProfile userProfile = new UserProfile();
        userProfile.setId(this.id);
        userProfile.setFirstname(this.firstname);
        userProfile.setLastname(this.lastname);
        userProfile.setCreationDate(this.creationDate);
        userProfile.setEmail(this.email);
        userProfile.setUsername(this.username);
        userProfile.setPassword(this.password);
        userProfile.setUsername(this.username);

        return userProfile;
    }
}

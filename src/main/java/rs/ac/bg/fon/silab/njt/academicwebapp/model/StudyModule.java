/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Anja
 */
@Entity
@Table(name = "study_module")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StudyModule.findAll", query = "SELECT s FROM StudyModule s")
    , @NamedQuery(name = "StudyModule.findByIdStudyModule", query = "SELECT s FROM StudyModule s WHERE s.idStudyModule = :idStudyModule")
    , @NamedQuery(name = "StudyModule.findByName", query = "SELECT s FROM StudyModule s WHERE s.name = :name")
    , @NamedQuery(name = "StudyModule.findByShortenedName", query = "SELECT s FROM StudyModule s WHERE s.shortenedName = :shortenedName")
    , @NamedQuery(name = "StudyModule.findByStartYear", query = "SELECT s FROM StudyModule s WHERE s.startYear = :startYear")
    , @NamedQuery(name = "StudyModule.findByStatus", query = "SELECT s FROM StudyModule s WHERE s.status = :status")})
public class StudyModule implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_study_module")
    private Long idStudyModule;
    @Size(max = 50)
    @Column(name = "name")
    private String name;
    @Size(max = 50)
    @Column(name = "shortened_name")
    private String shortenedName;
    @Column(name = "start_year")
    private Integer startYear;
    @Column(name = "status")
    private Boolean status;
    @JoinColumn(name = "id_faculty", referencedColumnName = "id_faculty")
    @ManyToOne
    private Faculty idFaculty;
    @JoinColumn(name = "id_study_program", referencedColumnName = "id_study_program")
    @ManyToOne
    private StudyProgram idStudyProgram;
    @OneToMany(mappedBy = "idStudyModule")
    private List<SemesterConfiguration> semesterConfigurationList;

    public StudyModule() {
    }

    public StudyModule(Long idStudyModule) {
        this.idStudyModule = idStudyModule;
    }

    public Long getIdStudyModule() {
        return idStudyModule;
    }

    public void setIdStudyModule(Long idStudyModule) {
        this.idStudyModule = idStudyModule;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortenedName() {
        return shortenedName;
    }

    public void setShortenedName(String shortenedName) {
        this.shortenedName = shortenedName;
    }

    public Integer getStartYear() {
        return startYear;
    }

    public void setStartYear(Integer startYear) {
        this.startYear = startYear;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Faculty getIdFaculty() {
        return idFaculty;
    }

    public void setIdFaculty(Faculty idFaculty) {
        this.idFaculty = idFaculty;
    }

    public StudyProgram getIdStudyProgram() {
        return idStudyProgram;
    }

    public void setIdStudyProgram(StudyProgram idStudyProgram) {
        this.idStudyProgram = idStudyProgram;
    }

    @XmlTransient
    public List<SemesterConfiguration> getSemesterConfigurationList() {
        return semesterConfigurationList;
    }

    public void setSemesterConfigurationList(List<SemesterConfiguration> semesterConfigurationList) {
        this.semesterConfigurationList = semesterConfigurationList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idStudyModule != null ? idStudyModule.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StudyModule)) {
            return false;
        }
        StudyModule other = (StudyModule) object;
        if ((this.idStudyModule == null && other.idStudyModule != null) || (this.idStudyModule != null && !this.idStudyModule.equals(other.idStudyModule))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "rs.ac.bg.fon.silab.njt.academicwebapp.model.StudyModule[ idStudyModule=" + idStudyModule + " ]";
    }
    
}

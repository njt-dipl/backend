/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.SemesterConfiguration;

/**
 *
 * @author Anja
 */
@Repository
public interface SemesterConfigurationRepository extends JpaRepository<SemesterConfiguration, Long> {

    @Override
    public <S extends SemesterConfiguration> S save(S entity);
    
    
    @Query(value = "SELECT sc FROM SemesterConfiguration sc WHERE sc.idStudyModule.idStudyModule=:idStudyModule"
            + " ORDER BY sc.semester, sc.position asc")
    List<SemesterConfiguration> findByStudyModule(@Param("idStudyModule") Long idStudyModule);


    @Query(value = "SELECT sc FROM SemesterConfiguration sc WHERE sc.idStudyProgram.idStudyProgram=:idStudyProgram AND "
            + "sc.idStudyModule.idStudyModule=:idStudyModule AND sc.semester=:numberOfSemester"
            + " ORDER BY sc.position asc")
    public List<SemesterConfiguration> findByStudyModuleAndSemester(@Param("idStudyProgram") Long idStudyProgram, @Param("idStudyModule") Long idStudyModule, @Param("numberOfSemester") int numberOfSemester);

//    @Query(value = "SELECT sc FROM SemesterConfiguration sc WHERE sc.semesterConfigurationPK.idStudyProgram=:idStudyProgram"
//            + " AND sc.studyModule.status"
//            + " AND sc.semesterConfigurationPK.semester=:numberOfSemester"
//            + " ORDER BY sc.semesterConfigurationPK.position asc")
//    public List<SemesterConfiguration> findByStudyProgramAndSemester(@Param("idStudyProgram")Long idStudyProgram, @Param("numberOfSemester") int numberOfSemester);
    
    
    @Query(value = "SELECT sc FROM SemesterConfiguration sc WHERE sc.idStudyProgram.idStudyProgram=:idStudyProgram AND "
            + "sc.idStudyModule.idStudyModule=:studyModuleId AND sc.semester=:semester AND "
            + "sc.position=:position")
    public SemesterConfiguration findConfiguration(@Param("idStudyProgram") Long idStudyProgram,
            @Param("studyModuleId") Long studyModuleId,
            @Param("semester") int semester,
            @Param("position") int position);

//    @Query(value = "DELETE sc FROM SemesterConfiguration sc WHERE sc.semesterConfigurationPK.idStudyProgram=:idStudyProgram AND "
//            + "sc.semesterConfigurationPK.idStudyModule=:studyModuleId AND sc.semesterConfigurationPK.semester=:semester AND "
//            + "sc.semesterConfigurationPK.position=:position")
//    public void delete(@Param("idStudyProgram") Long idStudyProgram,
//            @Param("studyModuleId") Long studyModuleId,
//            @Param("semester") int semester,
//            @Param("position") int position);
//    @Query(value = "UPDATE SemesterConfiguration sc SET sc.semesterConfigurationPK.position=:position0 "
//            + "WHERE sc.semesterConfigurationPK.idStudyProgram=:idStudyProgram AND "
//            + "sc.semesterConfigurationPK.idStudyModule=:idStudyModule AND sc.semesterConfigurationPK.semester=:semester AND "
//            + "sc.semesterConfigurationPK.position=:position")
//    public void update(long idStudyProgram, long idStudyModule, int semester, int position, int position0);

    
//    @Query(value = "UPDATE SemesterConfiguration sc "
//            + "SET sc.idSubject.idSubject=:subjectId "
//            + "WHERE sc.idSemesterConfiguration=:idConf")
//    public SemesterConfiguration update(@Param("idConf") Long idConf, @Param("subjectId") Long subjectId);

    
    @Query(value = "SELECT sc FROM SemesterConfiguration sc WHERE sc.idStudyProgram.idStudyProgram=:idStudyProgram"
            + " ORDER BY sc.semester, sc.position asc")
    public List<SemesterConfiguration> findByStudyProgram(@Param("idStudyProgram") Long idStudyProgram);
}

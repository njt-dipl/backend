/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Anja
 */
@Entity
@Table(name = "subject")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Subject.findAll", query = "SELECT s FROM Subject s")
    , @NamedQuery(name = "Subject.findByIdSubject", query = "SELECT s FROM Subject s WHERE s.idSubject = :idSubject")
    , @NamedQuery(name = "Subject.findByName", query = "SELECT s FROM Subject s WHERE s.name = :name")
    , @NamedQuery(name = "Subject.findByShortenedName", query = "SELECT s FROM Subject s WHERE s.shortenedName = :shortenedName")
    , @NamedQuery(name = "Subject.findByEspb", query = "SELECT s FROM Subject s WHERE s.espb = :espb")})
public class Subject implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_subject")
    private Long idSubject;
    @Size(max = 50)
    @Column(name = "name")
    private String name;
    @Size(max = 50)
    @Column(name = "shortened_name")
    private String shortenedName;
    @Column(name = "espb")
    private Integer espb;
    @JoinColumn(name = "id_faculty", referencedColumnName = "id_faculty")
    @ManyToOne
    private Faculty idFaculty;
    @JoinColumn(name = "id_subject_type", referencedColumnName = "id_subject_type")
    @ManyToOne
    private SubjectType idSubjectType;
    @OneToMany(mappedBy = "idSubject")
    private List<SubjectConfiguration> subjectConfigurationList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "subject")
    private List<FundOfClasses> fundOfClassesList;

    public Subject() {
    }

    public Subject(Long idSubject) {
        this.idSubject = idSubject;
    }

    public Long getIdSubject() {
        return idSubject;
    }

    public void setIdSubject(Long idSubject) {
        this.idSubject = idSubject;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortenedName() {
        return shortenedName;
    }

    public void setShortenedName(String shortenedName) {
        this.shortenedName = shortenedName;
    }

    public Integer getEspb() {
        return espb;
    }

    public void setEspb(Integer espb) {
        this.espb = espb;
    }

    public Faculty getIdFaculty() {
        return idFaculty;
    }

    public void setIdFaculty(Faculty idFaculty) {
        this.idFaculty = idFaculty;
    }

    public SubjectType getIdSubjectType() {
        return idSubjectType;
    }

    public void setIdSubjectType(SubjectType idSubjectType) {
        this.idSubjectType = idSubjectType;
    }

    @XmlTransient
    public List<SubjectConfiguration> getSubjectConfigurationList() {
        return subjectConfigurationList;
    }

    public void setSubjectConfigurationList(List<SubjectConfiguration> subjectConfigurationList) {
        this.subjectConfigurationList = subjectConfigurationList;
    }

    @XmlTransient
    public List<FundOfClasses> getFundOfClassesList() {
        return fundOfClassesList;
    }

    public void setFundOfClassesList(List<FundOfClasses> fundOfClassesList) {
        this.fundOfClassesList = fundOfClassesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSubject != null ? idSubject.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Subject)) {
            return false;
        }
        Subject other = (Subject) object;
        if ((this.idSubject == null && other.idSubject != null) || (this.idSubject != null && !this.idSubject.equals(other.idSubject))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "rs.ac.bg.fon.silab.njt.academicwebapp.model.Subject[ idSubject=" + idSubject + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.sevice;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.bg.fon.silab.njt.academicwebapp.exception.UnprocessableEntryException;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.TeachingForm;
import rs.ac.bg.fon.silab.njt.academicwebapp.repository.TeachingFormRepository;

/**
 *
 * @author Anja
 */
@Service
public class TeachingFormService {
    
    @Autowired
    private TeachingFormRepository teachingFormRepository;

    public List<TeachingForm> findAll() {
        return teachingFormRepository.findAll();
    }

    public TeachingForm fidByID(Long teachingFormId) {
        return teachingFormRepository.findById(teachingFormId).orElseThrow(UnprocessableEntryException::new);
    }
    
}

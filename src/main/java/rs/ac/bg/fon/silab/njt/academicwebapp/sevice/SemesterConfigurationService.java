/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.sevice;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.bg.fon.silab.njt.academicwebapp.exception.UnprocessableEntryException;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.SemesterConfiguration;
import rs.ac.bg.fon.silab.njt.academicwebapp.repository.SemesterConfigurationRepository;

/**
 *
 * @author Anja
 */
@Service
public class SemesterConfigurationService {

    @Autowired
    private SemesterConfigurationRepository configurationRepository;

    public void save(SemesterConfiguration configuration) {
        configurationRepository.save(configuration);
    }

    public List<SemesterConfiguration> findByStudyModule(Long idStudyModule) {
        return configurationRepository.findByStudyModule(idStudyModule);
    }

    public List<SemesterConfiguration> findByStudyModuleAndSemester(Long idStudyProgram, Long idStudyModule, int numberOfSemester) {
        return configurationRepository.findByStudyModuleAndSemester(idStudyProgram, idStudyModule, numberOfSemester);

    }

//    public List<SemesterConfiguration> findByStudyProgramAndSemester(Long idStudyProgram, int numberOfSemester) {
//        return configurationRepository.findByStudyProgramAndSemester(idStudyProgram, numberOfSemester);
//
//    }
    public SemesterConfiguration findConfiguration(Long idStudyProgram, Long studyModuleId, int semester, int position) {
        return configurationRepository.findConfiguration(idStudyProgram, studyModuleId, semester, position);
    }
//
//    public void delete(long idStudyProgram, long idStudyModule, int semester, int position) {
//        configurationRepository.delete(idStudyProgram,idStudyModule,semester,position);
//        
//    }

    public void delete(SemesterConfiguration semesterConfiguration) {
        configurationRepository.delete(semesterConfiguration);
    }

//    public void update(SemesterConfiguration config, int position) {
//        configurationRepository.update(config.getSemesterConfigurationPK().getIdStudyProgram(),
//                config.getSemesterConfigurationPK().getIdStudyModule(),
//                config.getSemesterConfigurationPK().getSemester(),
//                config.getSemesterConfigurationPK().getPosition(),
//                position);
//    }

    public SemesterConfiguration findById(Long idSemesterConfiguration) {
        return configurationRepository.findById(idSemesterConfiguration).orElseThrow(UnprocessableEntryException::new);
    }

//    public SemesterConfiguration update(Long idConf, Long subjectId) {
//        return configurationRepository.update(idConf, subjectId);
//    }

    public List<SemesterConfiguration> findByStudyProgram(Long idStudyProgram) {
        return configurationRepository.findByStudyProgram(idStudyProgram);
    }
}

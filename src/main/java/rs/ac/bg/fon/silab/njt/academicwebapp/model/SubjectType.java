/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Anja
 */
@Entity
@Table(name = "subject_type")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SubjectType.findAll", query = "SELECT s FROM SubjectType s")
    , @NamedQuery(name = "SubjectType.findByIdSubjectType", query = "SELECT s FROM SubjectType s WHERE s.idSubjectType = :idSubjectType")
    , @NamedQuery(name = "SubjectType.findByFullName", query = "SELECT s FROM SubjectType s WHERE s.fullName = :fullName")
    , @NamedQuery(name = "SubjectType.findByShortenedName", query = "SELECT s FROM SubjectType s WHERE s.shortenedName = :shortenedName")})
public class SubjectType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_subject_type")
    private Long idSubjectType;
    @Size(max = 255)
    @Column(name = "full_name")
    private String fullName;
    @Size(max = 50)
    @Column(name = "shortened_name")
    private String shortenedName;
    @OneToMany(mappedBy = "idSubjectType")
    private List<Subject> subjectList;
    @OneToMany(mappedBy = "idSubjectType")
    private List<SemesterConfiguration> semesterConfigurationList;

    public SubjectType() {
    }

    public SubjectType(Long idSubjectType) {
        this.idSubjectType = idSubjectType;
    }

    public Long getIdSubjectType() {
        return idSubjectType;
    }

    public void setIdSubjectType(Long idSubjectType) {
        this.idSubjectType = idSubjectType;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getShortenedName() {
        return shortenedName;
    }

    public void setShortenedName(String shortenedName) {
        this.shortenedName = shortenedName;
    }

    @XmlTransient
    public List<Subject> getSubjectList() {
        return subjectList;
    }

    public void setSubjectList(List<Subject> subjectList) {
        this.subjectList = subjectList;
    }

    @XmlTransient
    public List<SemesterConfiguration> getSemesterConfigurationList() {
        return semesterConfigurationList;
    }

    public void setSemesterConfigurationList(List<SemesterConfiguration> semesterConfigurationList) {
        this.semesterConfigurationList = semesterConfigurationList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSubjectType != null ? idSubjectType.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SubjectType)) {
            return false;
        }
        SubjectType other = (SubjectType) object;
        if ((this.idSubjectType == null && other.idSubjectType != null) || (this.idSubjectType != null && !this.idSubjectType.equals(other.idSubjectType))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "rs.ac.bg.fon.silab.njt.academicwebapp.model.SubjectType[ idSubjectType=" + idSubjectType + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rs.ac.bg.fon.silab.njt.academicwebapp.dto.SemesterConfigurationDTO;
import rs.ac.bg.fon.silab.njt.academicwebapp.dto.SubjectConfigurationDTO;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.SemesterConfiguration;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.StudyModule;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.StudyProgram;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.Subject;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.SubjectConfiguration;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.SubjectType;
import rs.ac.bg.fon.silab.njt.academicwebapp.sevice.SemesterConfigurationService;
import rs.ac.bg.fon.silab.njt.academicwebapp.sevice.StudyModuleService;
import rs.ac.bg.fon.silab.njt.academicwebapp.sevice.StudyProgramService;
import rs.ac.bg.fon.silab.njt.academicwebapp.sevice.SubjectConfigurationService;
import rs.ac.bg.fon.silab.njt.academicwebapp.sevice.SubjectTypeService;

/**
 *
 * @author Anja
 */
@RestController
@RequestMapping("/semesterConfiguration")
@CrossOrigin("http://localhost:4200")
public class SemesterConfigurationController {

    @Autowired
    private SemesterConfigurationService semesterConfigurationService;
    @Autowired
    private SubjectTypeService subjectTypeService;
    @Autowired
    private StudyModuleService studyModuleService;
    @Autowired
    private StudyProgramService studyProgramService;
    @Autowired
    private SubjectConfigurationService subjectConfigurationService;

    @PostMapping(path = "/save")
    public ResponseEntity<SemesterConfigurationDTO> save(@RequestBody SemesterConfigurationDTO dto) throws Exception {
        System.out.println("#############################################ULAZIM OVDE DTO SA STRANE: " + dto);
        SemesterConfiguration configuration = dto.convertToEntity();
        SubjectType subjectType = subjectTypeService.findByID(dto.getIdSubjectType());
        configuration.setIdSubjectType(subjectType);

        StudyModule studyModule = studyModuleService.findByID(dto.getIdStudyModule());
        configuration.setIdStudyModule(studyModule);

        StudyProgram sp = studyProgramService.findByID(dto.getIdStudyProgram());
        configuration.setIdStudyProgram(sp);

        if (studyModule.getStatus() == true) {
            semesterConfigurationService.save(configuration);
        }

        System.out.println("#############################################STATUS: " + studyModule.getStatus());
        if (studyModule.getStatus() == false) {
            configuration.setStatus(false);
            System.out.println("#############################################Usao u if ");
            //u pitanju je studijski program, a ne modul i treba da primenimo sve karakteristike konfiguracije na module s istim studyProgramId
            List<StudyModule> modules = studyModuleService.findByStudyProgram(studyModule.getIdStudyProgram().getIdStudyProgram());
            for (StudyModule module : modules) {
                configuration.setIdStudyModule(module);
                semesterConfigurationService.save(configuration);
            }
        }
        return new ResponseEntity<>(new SemesterConfigurationDTO(configuration), HttpStatus.OK);
    }

    @GetMapping(path = "/getByModule/{idStudyModule}")
    public ResponseEntity<List<SemesterConfigurationDTO>> getByStudyModule(@PathVariable("idStudyModule") Long idStudyModule) {
        System.out.println("***************************************************");
        System.out.println("USAO U METODU GET STUDY BY MODULE SA MODULOM : " + idStudyModule);
        List<SemesterConfigurationDTO> semesterConfigurations = new ArrayList<>();
        semesterConfigurationService.findByStudyModule(idStudyModule).stream()
                .map(SemesterConfigurationDTO::new)
                .forEachOrdered(semesterConfigurations::add);
        
        for (SemesterConfigurationDTO semesterConfiguration : semesterConfigurations) {
            System.out.println("#####1.");
            List<SubjectConfiguration> subjectConfigurations = subjectConfigurationService.findByIdSemesterConfiguration(semesterConfiguration.getIdSemesterConfiguration());
            System.out.println("#####2.");
            for (SubjectConfiguration subjectConfiguration : subjectConfigurations) {
                System.out.println("#####3.");
                semesterConfiguration.getSubjects().add(subjectConfiguration.getIdSubject());
                System.out.println("#####4. subjects: "+semesterConfiguration.getSubjects());
                System.out.println("PREDMET: "+subjectConfiguration.getIdSubject().getName());
            }   
            System.out.println("#####5.");
        }
        System.out.println("#####6.");
        
        ResponseEntity re = new ResponseEntity<>(semesterConfigurations, HttpStatus.OK);
        re.getHeaders().clear();
        System.out.println("#####7. re "+ re.toString());
        return re;
    }

    @GetMapping(path = "/getByModuleAndSemester/{idStudyProgram}/{idStudyModule}/{numberOfSemester}")
    public ResponseEntity<List<SemesterConfigurationDTO>> getByStudyModuleAndSemester(@PathVariable("idStudyProgram") Long idStudyProgram,
            @PathVariable("idStudyModule") Long idStudyModule,
            @PathVariable("numberOfSemester") int numberOfSemester) {
        System.out.println("***************************************************");
        System.out.println("USAO U METODU GET STUDY BY MODULE SA MODULOM : " + idStudyModule + " I"
                + " SA SEMESTROM : " + numberOfSemester);
        List<SemesterConfigurationDTO> semesterConfigurations = new ArrayList<>();
        semesterConfigurationService.findByStudyModuleAndSemester(idStudyProgram, idStudyModule, numberOfSemester).stream()
                .map(SemesterConfigurationDTO::new)
                .forEachOrdered(semesterConfigurations::add);
        return new ResponseEntity<>(semesterConfigurations, HttpStatus.OK);
    }

//    @GetMapping(path = "/getByProgramAndSemester/{idStudyProgram}/{numberOfSemester}")
//    public ResponseEntity<List<SemesterConfigurationDTO>> getByStudyProgram(@PathVariable("idStudyProgram") Long idStudyProgram,
//            @PathVariable("numberOfSemester") int numberOfSemester) {
//        System.out.println("***************************************************");
//        System.out.println("USAO U METODU GET STUDY BY PRGRAM SA MODULOM : " + idStudyProgram + " I"
//                + " SA SEMESTROM : " + numberOfSemester);
//        List<SemesterConfigurationDTO> semesterConfigurations = new ArrayList<>();
//        semesterConfigurationService.findByStudyProgramAndSemester(idStudyProgram, numberOfSemester).stream()
//                .map(SemesterConfigurationDTO::new)
//                .forEachOrdered(semesterConfigurations::add);
//        return new ResponseEntity<>(semesterConfigurations, HttpStatus.OK);
//    }
    //delete/${idStudyProgram}/${studyModuleId}/${semester}/${position}`);
    @DeleteMapping("delete/{idSemesterConfiguration}")
    public ResponseEntity<Void> delete(@PathVariable(value = "idSemesterConfiguration") Long idSemesterConfiguration) {
        SemesterConfiguration semesterConfiguration = semesterConfigurationService.findById(idSemesterConfiguration);
        Long studyProgramId = semesterConfiguration.getIdStudyProgram().getIdStudyProgram();
        int semester = semesterConfiguration.getSemester();

        if (semesterConfiguration.getStatus() == true) {
            Long studyModuleId = semesterConfiguration.getIdStudyModule().getIdStudyModule();
            semesterConfigurationService.delete(semesterConfiguration);
            List<SemesterConfiguration> moduleConfigurations = this.semesterConfigurationService.findByStudyModuleAndSemester(studyProgramId, studyModuleId, semester);
            int pos = 1;
            for (SemesterConfiguration moduleConfiguration : moduleConfigurations) {
                moduleConfiguration.setPosition(pos++);
                semesterConfigurationService.save(moduleConfiguration);
            }

        }
        if (semesterConfiguration.getStatus() == false) {
            List<StudyModule> modules = studyModuleService.findByStudyProgram(studyProgramId);
            for (StudyModule module : modules) {
                SemesterConfiguration semesterConfigurationModule = semesterConfigurationService.findConfiguration(studyProgramId, module.getIdStudyModule(), semesterConfiguration.getSemester(), semesterConfiguration.getPosition());
                semesterConfigurationService.delete(semesterConfigurationModule);

                List<SemesterConfiguration> moduleConfigurations = this.semesterConfigurationService.findByStudyModuleAndSemester(studyProgramId, module.getIdStudyModule(), semester);
                int pos = 1;
                for (SemesterConfiguration moduleConfiguration : moduleConfigurations) {
                    moduleConfiguration.setPosition(pos++);
                    semesterConfigurationService.save(moduleConfiguration);
                }
            }
        }

        return new ResponseEntity<Void>(HttpStatus.OK);
    }

//    private void refreshPositions(Long idStudyProgram, Long studyModuleId, int semester) {
//        System.out.println("!!!!!!!!!!!*******REFRESH*");
//        List<SemesterConfiguration> configs = semesterConfigurationService.findByStudyModuleAndSemester(idStudyProgram, studyModuleId, semester);
//        int position = 0;
//        for (SemesterConfiguration config : configs) {
//            position++;
////            config.getSemesterConfigurationPK().setPosition(position);
////            semesterConfigurationService.update(config, position);
//        }
//    }

    
//    @PostMapping(path = "/subjectConfiguration/save")
//    public ResponseEntity<SemesterConfigurationDTO> saveSubjectConfig(@RequestBody SubjectConfigurationDTO dto) throws Exception {
//        System.out.println("#######SUBJECT##################ULAZIM OVDE DTO SA STRANE: " + dto);
//        
//        SemesterConfiguration configuration = dto.convertToEntity();
//        
//        Long idConf = configuration.getIdSemesterConfiguration();
//        Long subjectId = configuration.getIdSubject().getIdSubject();
//        
//        StudyModule studyModule = studyModuleService.findByID(dto.getIdStudyModule());
////        configuration.setIdStudyModule(studyModule);
//
//        if (studyModule.getStatus() == true) {
//            semesterConfigurationService.update(idConf, subjectId);
//        }
//
//        System.out.println("#############################################STATUS: " + studyModule.getStatus());
//        if (studyModule.getStatus() == false) {
//            System.out.println("#############################################Usao u if ");
//            //u pitanju je studijski program, a ne modul i treba da primenimo sve karakteristike konfiguracije na module s istim studyProgramId
//            List<StudyModule> modules = studyModuleService.findByStudyProgram(studyModule.getIdStudyProgram().getIdStudyProgram());
//            for (StudyModule module : modules) {
//                configuration.setIdStudyModule(module);
//                semesterConfigurationService.update(idConf, subjectId);
//            }
//        }
//        return new ResponseEntity<>(new SemesterConfigurationDTO(configuration), HttpStatus.OK);
//    }
//    
//    
    
    
}

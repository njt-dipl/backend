/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.dto;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.City;

/**
 *
 * @author Anja
 */
@Getter
@Setter
@NoArgsConstructor
public class CityDTO {

    private Long postalCode;

    private String name;
    private String country;
    private List<UniversityDTO> universityList;
    private List<FacultyDTO> facultyList;

    public CityDTO(City city){
        this.postalCode = city.getPostalCode();
        this.name = city.getName();
        this.country = city.getCountry();
        this.universityList = new ArrayList<>();
        city.getUniversityList().stream()
                .map(UniversityDTO::new)
                .forEach(this.universityList::add);
        this.facultyList = new ArrayList<>();
        city.getFacultyList().stream()
                .map(FacultyDTO::new)
                .forEach(this.facultyList::add);
    }
    
    public City convertToEntity(){
        City city = new City();
        
        city.setPostalCode(this.postalCode);
        city.setName(this.name);
        city.setCountry(this.country);
        city.setUniversityList(new ArrayList<>());
        this.universityList.stream().map(UniversityDTO::convertToEntity)
                .forEach(city.getUniversityList()::add);  
        city.setFacultyList(new ArrayList<>());
        this.facultyList.stream().map(FacultyDTO::convertToEntity)
                .forEach(city.getFacultyList()::add);
        return city;
    }
    

}

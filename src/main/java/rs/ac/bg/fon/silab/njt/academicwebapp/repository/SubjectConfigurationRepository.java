/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.SubjectConfiguration;

/**
 *
 * @author Anja
 */
@Repository
public interface SubjectConfigurationRepository extends JpaRepository<SubjectConfiguration, Long> {

//    @Query(value = "SELECT sc FROM SubjectConfiguration sc WHERE sc..idStudyModule=:idStudyModule"
//            + " ORDER BY sc.subjectConfigurationPK.semester, sc.subjectConfigurationPK.position asc")
//    public List<SubjectConfiguration> findByStudyModule(@Param("idStudyModule") Long idStudyModule);

    @Query(value = "SELECT sc FROM SubjectConfiguration sc WHERE sc.idSemesterConfiguration.idSemesterConfiguration=:idSemesterConfiguration")
    public List<SubjectConfiguration> findByIdSemesterConfiguration(@Param("idSemesterConfiguration") Long idSemesterConfiguration);

}

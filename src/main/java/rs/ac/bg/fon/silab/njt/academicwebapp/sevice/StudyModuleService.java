/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.sevice;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.bg.fon.silab.njt.academicwebapp.exception.UnprocessableEntryException;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.StudyModule;
import rs.ac.bg.fon.silab.njt.academicwebapp.repository.StudyModuleRepository;

/**
 *
 * @author Korisnik
 */
@Service
public class StudyModuleService {

    @Autowired
    private StudyModuleRepository studyModuleRepository;

    public List<StudyModule> findByStudyProgram(Long idStudyProgram) {
        return studyModuleRepository.findByStudyProgram(idStudyProgram);
    }

    public List<StudyModule> findAll() {
        return studyModuleRepository.findAll();
    }

    public StudyModule findByID(Long id) {
        return studyModuleRepository.findById(id).orElseThrow(UnprocessableEntryException::new);
    }

    public StudyModule findStudyProgramInModulByID(Long idStudyProgram) {
        return studyModuleRepository.findStudyProgramInModulByID(idStudyProgram);
    }

}

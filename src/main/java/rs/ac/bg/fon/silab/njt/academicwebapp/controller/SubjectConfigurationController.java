/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rs.ac.bg.fon.silab.njt.academicwebapp.dto.SubjectConfigurationDTO;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.SemesterConfiguration;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.StudyModule;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.StudyProgram;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.Subject;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.SubjectConfiguration;
import rs.ac.bg.fon.silab.njt.academicwebapp.sevice.SemesterConfigurationService;
import rs.ac.bg.fon.silab.njt.academicwebapp.sevice.StudyModuleService;
import rs.ac.bg.fon.silab.njt.academicwebapp.sevice.StudyProgramService;
import rs.ac.bg.fon.silab.njt.academicwebapp.sevice.SubjectConfigurationService;
import rs.ac.bg.fon.silab.njt.academicwebapp.sevice.SubjectService;

/**
 *
 * @author Anja
 */
@RestController
@RequestMapping("/subjectConfiguration")
@CrossOrigin("http://localhost:4200")
public class SubjectConfigurationController {

    @Autowired
    private SubjectConfigurationService subjectConfigurationService;
    @Autowired
    private SemesterConfigurationService semesterConfigurationService;
    @Autowired
    private StudyModuleService studyModuleService;
    @Autowired
    private StudyProgramService studyProgramService;
    @Autowired
    private SubjectService subjectService;
    
    
    
    
    @PostMapping(path = "/save")
    public ResponseEntity<SubjectConfigurationDTO> save(@RequestBody SubjectConfigurationDTO dto) throws Exception {
        SubjectConfiguration configuration = dto.convertToEntity();
//        StudyModule sm = studyModuleService.findByID(dto.getIdStudyModule());
//        configuration.setStudyModule(sm);
        
//        StudyProgram sp = studyProgramService.findByID(dto.getIdStudyProgram());
//        configuration.setStudyProgram(sp);
        
        Subject s = subjectService.findByID(dto.getIdSubject());
        configuration.setIdSubject(s);
        
        SemesterConfiguration semesterConfig = semesterConfigurationService.findById(dto.getIdSemesterConfiguration());
        configuration.setIdSemesterConfiguration(semesterConfig);
        
        
        if (semesterConfig.getStatus() == true) {
            subjectConfigurationService.save(configuration);
        }
        
        if (semesterConfig.getStatus() == false) {
            System.out.println("#############################################Usao u if ");
            //u pitanju je studijski program, a ne modul i treba da primenimo sve karakteristike konfiguracije na module s istim studyProgramId
            List<SemesterConfiguration> configurations = semesterConfigurationService.findByStudyProgram(semesterConfig.getIdStudyProgram().getIdStudyProgram());
            for (SemesterConfiguration configur : configurations) {
                configuration.setIdSemesterConfiguration(configur);
                subjectConfigurationService.save(configuration);
            }
        }
        
//        
//        
//        SemesterConfiguration semConfig = semesterConfigurationService.findConfiguration(
//                configuration.getSubjectConfigurationPK().getIdStudyProgram(), 
//                configuration.getSubjectConfigurationPK().getIdStudyModule(), 
//                configuration.getSubjectConfigurationPK().getSemester(), 
//                configuration.getSubjectConfigurationPK().getPosition());
//
//
//
////            subjectConfigurationService.save(configuration);
        return new ResponseEntity<>(new SubjectConfigurationDTO(configuration), HttpStatus.OK);
    }
//    
//    
//    
//    @GetMapping(path = "/getByModule/{idStudyModule}")
//    public ResponseEntity<List<SubjectConfigurationDTO>> getByStudyModule(@PathVariable("idStudyModule") Long idStudyModule) {
//       
//        
//       
//        List<SubjectConfigurationDTO> subjectConfigurations = new ArrayList<>();
//        subjectConfigurationService.findByStudyModule(idStudyModule).stream()
//                .map(SubjectConfigurationDTO::new)
//                .forEachOrdered(subjectConfigurations::add);
//        return new ResponseEntity<>(subjectConfigurations, HttpStatus.OK);
//    }
    
    
    



}
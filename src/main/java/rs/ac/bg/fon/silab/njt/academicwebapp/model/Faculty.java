/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Anja
 */
@Entity
@Table(name = "faculty")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Faculty.findAll", query = "SELECT f FROM Faculty f")
    , @NamedQuery(name = "Faculty.findByIdFaculty", query = "SELECT f FROM Faculty f WHERE f.idFaculty = :idFaculty")
    , @NamedQuery(name = "Faculty.findByName", query = "SELECT f FROM Faculty f WHERE f.name = :name")
    , @NamedQuery(name = "Faculty.findByShortenedName", query = "SELECT f FROM Faculty f WHERE f.shortenedName = :shortenedName")
    , @NamedQuery(name = "Faculty.findByAddress", query = "SELECT f FROM Faculty f WHERE f.address = :address")
    , @NamedQuery(name = "Faculty.findByContact", query = "SELECT f FROM Faculty f WHERE f.contact = :contact")})
public class Faculty implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_faculty")
    private Long idFaculty;
    @Size(max = 50)
    @Column(name = "name")
    private String name;
    @Size(max = 50)
    @Column(name = "shortened_name")
    private String shortenedName;
    @Size(max = 255)
    @Column(name = "address")
    private String address;
    @Size(max = 255)
    @Column(name = "contact")
    private String contact;
    @OneToMany(mappedBy = "idFaculty")
    private List<StudyModule> studyModuleList;
    @OneToMany(mappedBy = "idFaculty")
    private List<Subject> subjectList;
    @OneToMany(mappedBy = "idFaculty")
    private List<StudyProgram> studyProgramList;
    @JoinColumn(name = "postal_code", referencedColumnName = "postal_code")
    @ManyToOne
    private City postalCode;
    @JoinColumn(name = "id_grouping", referencedColumnName = "id_grouping")
    @ManyToOne
    private FacultyGrouping idGrouping;
    @JoinColumn(name = "id_university", referencedColumnName = "id_university")
    @ManyToOne
    private University idUniversity;

    public Faculty() {
    }

    public Faculty(Long idFaculty) {
        this.idFaculty = idFaculty;
    }

    public Long getIdFaculty() {
        return idFaculty;
    }

    public void setIdFaculty(Long idFaculty) {
        this.idFaculty = idFaculty;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortenedName() {
        return shortenedName;
    }

    public void setShortenedName(String shortenedName) {
        this.shortenedName = shortenedName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    @XmlTransient
    public List<StudyModule> getStudyModuleList() {
        return studyModuleList;
    }

    public void setStudyModuleList(List<StudyModule> studyModuleList) {
        this.studyModuleList = studyModuleList;
    }

    @XmlTransient
    public List<Subject> getSubjectList() {
        return subjectList;
    }

    public void setSubjectList(List<Subject> subjectList) {
        this.subjectList = subjectList;
    }

    @XmlTransient
    public List<StudyProgram> getStudyProgramList() {
        return studyProgramList;
    }

    public void setStudyProgramList(List<StudyProgram> studyProgramList) {
        this.studyProgramList = studyProgramList;
    }

    public City getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(City postalCode) {
        this.postalCode = postalCode;
    }

    public FacultyGrouping getIdGrouping() {
        return idGrouping;
    }

    public void setIdGrouping(FacultyGrouping idGrouping) {
        this.idGrouping = idGrouping;
    }

    public University getIdUniversity() {
        return idUniversity;
    }

    public void setIdUniversity(University idUniversity) {
        this.idUniversity = idUniversity;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idFaculty != null ? idFaculty.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Faculty)) {
            return false;
        }
        Faculty other = (Faculty) object;
        if ((this.idFaculty == null && other.idFaculty != null) || (this.idFaculty != null && !this.idFaculty.equals(other.idFaculty))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "rs.ac.bg.fon.silab.njt.academicwebapp.model.Faculty[ idFaculty=" + idFaculty + " ]";
    }
    
}

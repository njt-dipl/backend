package rs.ac.bg.fon.silab.njt.academicwebapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import rs.ac.bg.fon.silab.njt.academicwebapp.model.UserProfile;

import java.util.List;
import java.util.Optional;

public interface UserProfileRepository extends JpaRepository<UserProfile, Long> {

    <E extends UserProfile> Optional<E> findByEmail(String email);

    <E extends UserProfile> Optional<E> findById(Long id);

    List<UserProfile> findAll();

    <E extends UserProfile> Optional<E> findByUsernameIgnoreCase(String username);

    <E extends UserProfile> Optional<E> findByVerificationNumber(String verificationNumber);

    <E extends UserProfile> Optional<E> findByEmailIgnoreCase(String email);

    List<UserProfile> findAllByFirstnameAndLastname(String firstName, String lastName);

    <E extends UserProfile> E save(E userProfile);

}

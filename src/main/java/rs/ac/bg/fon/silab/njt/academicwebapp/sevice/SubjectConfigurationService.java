/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.sevice;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.SubjectConfiguration;
import rs.ac.bg.fon.silab.njt.academicwebapp.repository.SubjectConfigurationRepository;

/**
 *
 * @author Anja
 */
@Service
public class SubjectConfigurationService {

    @Autowired
    private SubjectConfigurationRepository subjectConfigurationRepository;
    
    public void save(SubjectConfiguration configuration) {
        subjectConfigurationRepository.save(configuration);
    }

//    public List<SubjectConfiguration> findByStudyModule(Long idStudyModule) {
//         return subjectConfigurationRepository.findByStudyModule(idStudyModule);
//    }

    public List<SubjectConfiguration> findByIdSemesterConfiguration(Long idSemesterConfiguration) {
        return subjectConfigurationRepository.findByIdSemesterConfiguration(idSemesterConfiguration);
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.SemesterConfiguration;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.StudyModule;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.Subject;

/**
 *
 * @author Anja
 */
@Getter
@Setter
@NoArgsConstructor
public class SemesterConfigurationDTO {
   
    private Long idSemesterConfiguration;
    private Long idStudyProgram;
    private int position;
    private int semester;
    private int espb;
    private String mandatory;
    private Long idStudyModule;
    private Long idSubjectType;
    private boolean status;
    private String subjectTypeName;
//    @JsonIgnore
    private List<Subject> subjects;
    
    public SemesterConfigurationDTO(SemesterConfiguration semesterConfiguration) {
        this.idSemesterConfiguration = semesterConfiguration.getIdSemesterConfiguration();
        this.idStudyProgram = semesterConfiguration.getIdStudyProgram().getIdStudyProgram();
        this.position = semesterConfiguration.getPosition();
        this.semester = semesterConfiguration.getSemester();
        this.idSubjectType = semesterConfiguration.getIdSubjectType().getIdSubjectType();
        this.espb = semesterConfiguration.getEspb();
        this.mandatory = semesterConfiguration.getMandatory();
        this.idStudyModule = semesterConfiguration.getIdStudyModule().getIdStudyModule();
        this.status = semesterConfiguration.getStatus();
        this.subjectTypeName = semesterConfiguration.getIdSubjectType().getFullName();
        this.subjects = new ArrayList<>();
    }
   
    public SemesterConfiguration convertToEntity() {
        SemesterConfiguration semesterConfiguration = new SemesterConfiguration();
        semesterConfiguration.setIdSemesterConfiguration(this.idSemesterConfiguration);
//        semesterConfiguration.setIdStudyProgram(this.idStudyProgram);
//        semesterConfiguration.setIdStudyModule(this.idStudyModule);
        semesterConfiguration.setPosition(this.position);
        semesterConfiguration.setSemester(this.semester);
        semesterConfiguration.setMandatory(this.mandatory);
        semesterConfiguration.setEspb(this.espb);
        semesterConfiguration.setStatus(this.status);
//        semesterConfiguration.setIdSubject(this.idSubject);
       
//        semesterConfiguration.setIdStudyModule(new StudyModule(idStudyModule));
        return semesterConfiguration;
    }

    @Override
    public String toString() {
        return "SemesterConfigurationDTO{" + "idSemesterConfiguration=" + idSemesterConfiguration + ", idStudyProgram=" + idStudyProgram + ", position=" + position + ", semester=" + semester + ", espb=" + espb + ", mandatory=" + mandatory + ", idStudyModule=" + idStudyModule + ", idSubjectType=" + idSubjectType + ", status=" + status + ", subjectTypeName=" + subjectTypeName + ", subjects=" + subjects + '}';
    }

    
    



    
    
       
}

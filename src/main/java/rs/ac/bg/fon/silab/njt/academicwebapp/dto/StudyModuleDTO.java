/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.Faculty;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.StudyModule;

/**
 *
 * @author Korisnik
 */
@Getter
@Setter
@NoArgsConstructor
public class StudyModuleDTO {

    private Long idStudyModule;
    private String name;
    private String shortenedName;
    private int startYear;
    private boolean status;
    private Long idFaculty;
    private Long idStudyProgram;

    public StudyModuleDTO(StudyModule studyModule) {
        this.idStudyModule = studyModule.getIdStudyModule();
        this.name = studyModule.getName();
        this.shortenedName = studyModule.getShortenedName();
        this.startYear = studyModule.getStartYear();
        this.status = studyModule.getStatus();
        this.idFaculty = studyModule.getIdFaculty().getIdFaculty();
        this.idStudyProgram = studyModule.getIdStudyProgram().getIdStudyProgram();

    }

    public StudyModule convertToEntity() {
        StudyModule studyModule = new StudyModule();
        studyModule.setName(this.name);
        studyModule.setShortenedName(this.shortenedName);
        studyModule.setStartYear(this.startYear);
        studyModule.setStatus(this.status);
        studyModule.setIdFaculty(new Faculty(this.idFaculty));
        return studyModule;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Anja
 */
@Entity
@Table(name = "subject_configuration")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SubjectConfiguration.findAll", query = "SELECT s FROM SubjectConfiguration s")
    , @NamedQuery(name = "SubjectConfiguration.findByIdSubjectConfiguration", query = "SELECT s FROM SubjectConfiguration s WHERE s.idSubjectConfiguration = :idSubjectConfiguration")})
public class SubjectConfiguration implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_subject_configuration")
    private Long idSubjectConfiguration;
    @JoinColumn(name = "id_semester_configuration", referencedColumnName = "id_semester_configuration")
    @ManyToOne
    private SemesterConfiguration idSemesterConfiguration;
    @JoinColumn(name = "id_subject", referencedColumnName = "id_subject")
    @ManyToOne
    private Subject idSubject;

    public SubjectConfiguration() {
    }

    public SubjectConfiguration(Long idSubjectConfiguration) {
        this.idSubjectConfiguration = idSubjectConfiguration;
    }

    public Long getIdSubjectConfiguration() {
        return idSubjectConfiguration;
    }

    public void setIdSubjectConfiguration(Long idSubjectConfiguration) {
        this.idSubjectConfiguration = idSubjectConfiguration;
    }

    public SemesterConfiguration getIdSemesterConfiguration() {
        return idSemesterConfiguration;
    }

    public void setIdSemesterConfiguration(SemesterConfiguration idSemesterConfiguration) {
        this.idSemesterConfiguration = idSemesterConfiguration;
    }

    public Subject getIdSubject() {
        return idSubject;
    }

    public void setIdSubject(Subject idSubject) {
        this.idSubject = idSubject;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSubjectConfiguration != null ? idSubjectConfiguration.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SubjectConfiguration)) {
            return false;
        }
        SubjectConfiguration other = (SubjectConfiguration) object;
        if ((this.idSubjectConfiguration == null && other.idSubjectConfiguration != null) || (this.idSubjectConfiguration != null && !this.idSubjectConfiguration.equals(other.idSubjectConfiguration))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "rs.ac.bg.fon.silab.njt.academicwebapp.model.SubjectConfiguration[ idSubjectConfiguration=" + idSubjectConfiguration + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.Subject;

/**
 *
 * @author Anja
 */
@Repository
public interface SubjectRepository extends JpaRepository<Subject, Long>{
 
    List<Subject> findAll();
    

    @Query(value = "SELECT s FROM Subject s WHERE s.idFaculty.idFaculty=:idFaculty")
    public List<Subject> findAllFromFaculty(@Param("idFaculty") Long idFaculty);

    @Query(value = "SELECT s FROM Subject s WHERE s.idSubjectType.idSubjectType =:idSubjectType")
    public List<Subject> findAllForSubjectType(Long idSubjectType);
}

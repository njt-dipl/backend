package rs.ac.bg.fon.silab.njt.academicwebapp.exception;

import lombok.NoArgsConstructor;

/**
 * User profile authorization fails exception
 */
@NoArgsConstructor
public class AuthorizationException extends RuntimeException {

    public AuthorizationException(String message) {
        super(message);
    }
}

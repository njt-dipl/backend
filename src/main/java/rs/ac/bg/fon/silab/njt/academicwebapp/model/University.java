/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Anja
 */
@Entity
@Table(name = "university")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "University.findAll", query = "SELECT u FROM University u")
    , @NamedQuery(name = "University.findByIdUniversity", query = "SELECT u FROM University u WHERE u.idUniversity = :idUniversity")
    , @NamedQuery(name = "University.findByName", query = "SELECT u FROM University u WHERE u.name = :name")
    , @NamedQuery(name = "University.findByImgPath", query = "SELECT u FROM University u WHERE u.imgPath = :imgPath")})
public class University implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_university")
    private Long idUniversity;
    @Size(max = 255)
    @Column(name = "name")
    private String name;
    @Size(max = 255)
    @Column(name = "img_path")
    private String imgPath;
    @JoinColumn(name = "postal_code", referencedColumnName = "postal_code")
    @ManyToOne
    private City postalCode;
    @OneToMany(mappedBy = "idUniversity")
    private List<Faculty> facultyList;

    public University() {
    }

    public University(Long idUniversity) {
        this.idUniversity = idUniversity;
    }

    public Long getIdUniversity() {
        return idUniversity;
    }

    public void setIdUniversity(Long idUniversity) {
        this.idUniversity = idUniversity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public City getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(City postalCode) {
        this.postalCode = postalCode;
    }

    @XmlTransient
    public List<Faculty> getFacultyList() {
        return facultyList;
    }

    public void setFacultyList(List<Faculty> facultyList) {
        this.facultyList = facultyList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUniversity != null ? idUniversity.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof University)) {
            return false;
        }
        University other = (University) object;
        if ((this.idUniversity == null && other.idUniversity != null) || (this.idUniversity != null && !this.idUniversity.equals(other.idUniversity))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "rs.ac.bg.fon.silab.njt.academicwebapp.model.University[ idUniversity=" + idUniversity + " ]";
    }
    
}

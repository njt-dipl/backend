/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.SubjectType;

/**
 *
 * @author Anja
 */
@Getter
@Setter
@NoArgsConstructor
public class SubjectTypeDTO {
    private Long id;
    private String fullName;
    private String shortenedName;
    
    public SubjectTypeDTO(SubjectType st){
        this.id = st.getIdSubjectType();
        this.fullName = st.getFullName();
        this.shortenedName = st.getShortenedName();
    }
    
    public SubjectType convertToEntity() {
        SubjectType type = new SubjectType();
        type.setIdSubjectType(this.id);
        type.setFullName(this.fullName);
        type.setShortenedName(this.shortenedName);
        return type;
    }
    
}

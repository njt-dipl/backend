/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rs.ac.bg.fon.silab.njt.academicwebapp.dto.UniversityDTO;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.University;
import rs.ac.bg.fon.silab.njt.academicwebapp.sevice.UniversityService;

/**
 *
 * @author Korisnik
 */
@RestController
@RequestMapping("/university")
@CrossOrigin("http://localhost:4200")
public class UniversityController {

    @Autowired
    private UniversityService universityService;

    @GetMapping("/getAll")
    public ResponseEntity<List<UniversityDTO>> getAll() {
        List<UniversityDTO> universities = new ArrayList<>();
        universityService.findAll().stream()
                .map(UniversityDTO::new)
            .forEachOrdered(universities::add);

        return new ResponseEntity<>(universities, HttpStatus.OK);
    }
}
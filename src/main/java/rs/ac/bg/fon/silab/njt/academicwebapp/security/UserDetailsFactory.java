package rs.ac.bg.fon.silab.njt.academicwebapp.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import rs.ac.bg.fon.silab.njt.academicwebapp.model.UserProfile;

public class UserDetailsFactory {

	/**
	 * Private constructor created to hide the implicit public constructor
	 */
	UserDetailsFactory() {
		throw new IllegalStateException("This class shouldn't be instantiated.");
	}

	/**
	 * Creates SpringSecurityUserProfile from a UserProfile instance.
	 *
	 * @param userProfile UserProfile instance
	 * @return {@link SpringSecurityUserProfile} Instance
	 */
	public static SpringSecurityUserProfile create(UserProfile userProfile) {
		return new SpringSecurityUserProfile(userProfile.getId(), userProfile.getUsername(), userProfile.getPassword(),
				userProfile.getEmail());
	}
}

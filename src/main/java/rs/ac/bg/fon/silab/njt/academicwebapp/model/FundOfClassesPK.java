/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Anja
 */
@Embeddable
public class FundOfClassesPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_subject")
    private long idSubject;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_teaching_form")
    private long idTeachingForm;

    public FundOfClassesPK() {
    }

    public FundOfClassesPK(long idSubject, long idTeachingForm) {
        this.idSubject = idSubject;
        this.idTeachingForm = idTeachingForm;
    }

    public long getIdSubject() {
        return idSubject;
    }

    public void setIdSubject(long idSubject) {
        this.idSubject = idSubject;
    }

    public long getIdTeachingForm() {
        return idTeachingForm;
    }

    public void setIdTeachingForm(long idTeachingForm) {
        this.idTeachingForm = idTeachingForm;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idSubject;
        hash += (int) idTeachingForm;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FundOfClassesPK)) {
            return false;
        }
        FundOfClassesPK other = (FundOfClassesPK) object;
        if (this.idSubject != other.idSubject) {
            return false;
        }
        if (this.idTeachingForm != other.idTeachingForm) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "rs.ac.bg.fon.silab.njt.academicwebapp.model.FundOfClassesPK[ idSubject=" + idSubject + ", idTeachingForm=" + idTeachingForm + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.sevice;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.FundOfClasses;
import rs.ac.bg.fon.silab.njt.academicwebapp.repository.FundOfClassesRepository;

/**
 *
 * @author Anja
 */
@Service
public class FundOfClassesService {
    
    @Autowired
    private FundOfClassesRepository fundOfClassesRepository;

    public void create(FundOfClasses foc) {
        fundOfClassesRepository.save(foc);
    }

    public List<FundOfClasses> findAll() {
        return fundOfClassesRepository.findAll();
    }

    public List<FundOfClasses> findAllForSubject(Long id) {
        return fundOfClassesRepository.findAllForSubject(id);
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rs.ac.bg.fon.silab.njt.academicwebapp.dto.FacultyGroupingDTO;
import rs.ac.bg.fon.silab.njt.academicwebapp.dto.StudyProgramDTO;
import rs.ac.bg.fon.silab.njt.academicwebapp.sevice.StudyProgramService;

/**
 *
 * @author Korisnik
 */
@RestController
@RequestMapping("/studyProgram")
@CrossOrigin("http://localhost:4200")
public class StudyProgramController {

    @Autowired
    private StudyProgramService studyProgramService;

    @GetMapping("/getByFaculty/{facultyId}")
    public ResponseEntity<List<StudyProgramDTO>> getByFaculty(@PathVariable("facultyId") Long facultyId) {
        List<StudyProgramDTO> studyPrograms = new ArrayList<>();
        studyProgramService.findByFacultyId(facultyId).stream()
                .map(StudyProgramDTO::new)
                .forEachOrdered(studyPrograms::add);

        return new ResponseEntity<>(studyPrograms, HttpStatus.OK);
    }

}

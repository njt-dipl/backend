/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.dto;

import java.util.HashMap;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.Subject;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.SubjectType;

/**
 *
 * @author Anja
 */
@Getter
@Setter
@NoArgsConstructor
public class SubjectDTO {

    private Long idSubject;
    private String name;
    private String shortenedName;
    private Long idSubjectType;
    private Integer espb;
    private Long facultyId;

    public SubjectDTO(Subject subject) {
        this.idSubject = subject.getIdSubject();
        this.name = subject.getName();
        this.shortenedName = subject.getShortenedName();
        this.idSubjectType = subject.getIdSubjectType().getIdSubjectType();
        this.espb = subject.getEspb();
        this.facultyId = subject.getIdFaculty().getIdFaculty();
    }
    
    public Subject convertToEntity() {
        Subject subject = new Subject();
        subject.setIdSubject(this.idSubject);
        subject.setName(this.name);
        subject.setShortenedName(this.shortenedName);
//        subject.setIdSubjectType(idSubjectType);
        subject.setEspb(this.espb);
        return subject;
    }

    @Override
    public String toString() {
        return "SubjectDTO{" + "idSubject=" + idSubject + ", name=" + name + ", shortenedName=" + shortenedName + ", idSubjectType=" + idSubjectType + ", espb=" + espb + ", facultyId=" + facultyId + '}';
    }

    
    
    
}

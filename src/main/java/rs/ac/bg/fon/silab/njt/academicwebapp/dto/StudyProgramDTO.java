/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.Faculty;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.LevelOfStudy;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.StudyProgram;

/**
 *
 * @author Korisnik
 */
@Getter
@Setter
@NoArgsConstructor
public class StudyProgramDTO {

    private Long idStudyProgram;
    private String name;
    private String shortenedName;
    private int numberOfSemester;
    private Long levelOfStudy;
    private Long idFaculty;
    private List<StudyModuleDTO> studyModules;

    public StudyProgramDTO(StudyProgram studyProgram) {
        this.idStudyProgram = studyProgram.getIdStudyProgram();
        this.name = studyProgram.getName();
        this.shortenedName = studyProgram.getShortenedName();
        this.numberOfSemester = studyProgram.getNumberOfSemesters();
        this.levelOfStudy = studyProgram.getLevelOfStudy().getId();
        this.idFaculty = studyProgram.getIdFaculty().getIdFaculty();
//        this.universityId = faculty.getFacultyPK().getIdUniversity();
//        this.facultyGroupingId = faculty.getFacultyPK().getIdGrouping();

//        if (faculty.getIdUniversity() == null) {
//            this.universityId = null;
//        } else {
//            this.universityId = faculty.getIdUniversity().getIdUniversity();
//        }
//        if (faculty.getIdGrouping() == null) {
//            this.facultyGroupingId = null;
//        } else {
//            this.facultyGroupingId = faculty.getIdGrouping().getIdGrouping();
//        }
//        if (faculty.getPostalCode() == null) {
//            this.postalCode = null;
//        } else {
//            this.postalCode = faculty.getPostalCode().getPostalCode();
//        }
    }

    public StudyProgram convertToEntity() {
        StudyProgram studyProgram = new StudyProgram();
        studyProgram.setName(this.name);
        studyProgram.setShortenedName(this.shortenedName);
        studyProgram.setNumberOfSemesters(this.numberOfSemester);
        studyProgram.setLevelOfStudy(new LevelOfStudy(this.levelOfStudy));
        studyProgram.setIdFaculty(new Faculty(this.idFaculty));
//        studyProgram.setStudyModuleList(this.studyModules);
//        faculty.setPostalCode(new City(postalCode));
//        faculty.setFacultyGrouping(new FacultyGrouping(facultyGroupingId));
//        faculty.setUniversity(new University(universityId));
//        faculty.setFacultyPK(new FacultyPK(universityId, facultyGroupingId, id));
        return studyProgram;
    }

}

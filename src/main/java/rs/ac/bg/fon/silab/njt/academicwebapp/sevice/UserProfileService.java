package rs.ac.bg.fon.silab.njt.academicwebapp.sevice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

//import javax.mail.MessagingException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import javassist.NotFoundException;
import rs.ac.bg.fon.silab.njt.academicwebapp.exception.AuthorizationException;
import rs.ac.bg.fon.silab.njt.academicwebapp.exception.UnprocessableEntryException;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.UserProfile;
import rs.ac.bg.fon.silab.njt.academicwebapp.repository.UserProfileRepository;

@Service
public class UserProfileService {

    @Autowired
    private final UserProfileRepository userProfileRepository;
    @Autowired
    private final EmailService emailService;

    @Autowired
    public UserProfileService(UserProfileRepository userProfileRepository, EmailService emailService) {
        this.userProfileRepository = userProfileRepository;
        this.emailService = emailService;
    }

    public UserProfile findByEmail(String email) {
        return this.userProfileRepository.findByEmail(email).orElseThrow(null);
    }

    public void checkIfEmailIsTaken(String email) {
        this.userProfileRepository.findByEmailIgnoreCase(email)
                .ifPresent(userProfile -> {
                    throw new UnprocessableEntryException(String.format("Email '%s' is already taken", email));
                });
    }

    public List<UserProfile> findAllByFirstNameAndLastName(String firstName, String lastName) {
        return this.userProfileRepository.findAllByFirstnameAndLastname(firstName, lastName);
    }

    public UserProfile save(UserProfile userProfile) {
        return this.userProfileRepository.save(userProfile);
    }

    public UserProfile findByID(Long id) {
//        return this.userProfileRepository.findById(id).orElseThrow(NotFoundException::new);
        return null;

    }

    public List<UserProfile> findAll() {
        return this.userProfileRepository.findAll();
    }

    public UserProfile findByUsername(String username) {
        return this.userProfileRepository.findByUsernameIgnoreCase(username)
                .orElseThrow(AuthorizationException::new);
    }

    public void checkIfUsernameIsTaken(String username) {
        Optional<UserProfile> up = userProfileRepository.findByUsernameIgnoreCase(username);
        this.userProfileRepository.findByUsernameIgnoreCase(username)
                .ifPresent(userProfile -> {
                    throw new UnprocessableEntryException(String.format("Username '%s' is already taken", username));
                });
    }

    /**
     * Finds the currently active {@link UserProfile}. If no user is logged in,
     * the method throws a {@link Exception}.
     *
     * @return Currently active {@link UserProfile}
     * @throws Exception Exception thrown in case the authentication fails.
     */
    public UserProfile findCurrentUser() throws Exception {
        final UsernamePasswordAuthenticationToken authentication = (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            throw new Exception("You don't have permission to access this method on the server.");
        }

        final UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        return this.findByUsername(userDetails.getUsername());
    }

    /**
     * Verifies the user's account and removes previously generated verification
     * code.
     *
     * @param verificationCode verification code of the user to be verified
     * @return verified {@link UserProfile}
     */
    public UserProfile verifyUserProfile(String verificationNumber) {
        final UserProfile registeredUser = this.userProfileRepository.findByVerificationNumber(verificationNumber)
                .orElseThrow(() -> new UnprocessableEntryException("No user found with specified verification code."));

        registeredUser.setVerified(true);
        registeredUser.setVerificationNumber(null);

        return this.userProfileRepository.save(registeredUser);
    }

    /**
     * Updates the {@link UserProfile} data
     *
     * @param updatedUser {@link UserProfile} instance containing updated data
     * @return updated {@link UserProfile} instance
     */
    public UserProfile update(UserProfile updatedUser) {
        final UserProfile userProfile = this.findByUsername(updatedUser.getUsername());

        if (!userProfile.getEmail().equals(updatedUser.getEmail())) {
            this.checkIfEmailIsTaken(updatedUser.getEmail());
        }

        userProfile.setFirstname(updatedUser.getFirstname());
        userProfile.setLastname(updatedUser.getLastname());
        userProfile.setEmail(updatedUser.getEmail());
        userProfile.setCreationDate(updatedUser.getCreationDate());
        userProfile.setVerified(updatedUser.getVerified());
        userProfile.setVerificationNumber(updatedUser.getVerificationNumber());
        userProfile.setUsername(updatedUser.getUsername());

        return this.userProfileRepository.save(userProfile);
    }

    /**
     * Creates a new User profile and sends verification email
     *
     * @param userProfile {@link UserProfile} instance to be created
     * @return created {@link UserProfile} instance
     */
    public UserProfile create(UserProfile userProfile) throws Exception {
        userProfile.setVerified(false);
        userProfile.setVerificationNumber(UUID.randomUUID().toString());

        this.checkIfUsernameIsTaken(userProfile.getUsername());
        this.checkIfEmailIsTaken(userProfile.getEmail());

        final UserProfile savedUserProfile = this.save(userProfile);
        this.emailService.sendVerificationEmail(userProfile);

        return savedUserProfile;
    }

    public String successResponse() {
        return emailService.successTemplate();
    }

}

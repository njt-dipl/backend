package rs.ac.bg.fon.silab.njt.academicwebapp.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.UserProfile;

@Getter
@Setter
@NoArgsConstructor
public class UserProfileBasicDTO {

    private Long id;

    @NotBlank(message = "first name cannot be blank")
    private String firstname;

    @NotBlank(message = "last name cannot be blank")
    private String lastname;

    @NotBlank(message = "email cannot be blank")
    private String email;

    @NotBlank(message = "username cannot be blank")
    private String username;

    @NotBlank(message = "password cannot be blank")
    private String password;

    public UserProfileBasicDTO(UserProfile userProfile) {
        this.id = userProfile.getId();
        this.firstname = userProfile.getFirstname();
        this.lastname = userProfile.getLastname();
        this.email = userProfile.getEmail();
        this.password = userProfile.getPassword();
        this.username = userProfile.getUsername();
    }
}

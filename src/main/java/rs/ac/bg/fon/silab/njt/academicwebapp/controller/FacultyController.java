/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rs.ac.bg.fon.silab.njt.academicwebapp.dto.FacultyDTO;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.Faculty;
import rs.ac.bg.fon.silab.njt.academicwebapp.sevice.FacultyService;

/**
 *
 * @author Anja
 */
@RestController
@RequestMapping("/faculty")
@CrossOrigin("http://localhost:4200")
public class FacultyController {

    @Autowired
    private FacultyService facultyService;

    @GetMapping("/getAll")
    public ResponseEntity<List<FacultyDTO>> getAll() {
        List<FacultyDTO> faculties = new ArrayList<>();
        facultyService.findAll().stream()
                .map(FacultyDTO::new)
                .forEachOrdered(faculties::add);

        for (FacultyDTO faculty : faculties) {
            System.out.println(faculty);

        }

        return new ResponseEntity<>(faculties, HttpStatus.OK);
    }
    
    //${API_URL}/faculty/${facultyId}
    
@GetMapping("/{id}")
    public ResponseEntity<FacultyDTO> findByID(
            @PathVariable("id") Long id) {
        Faculty faculty = facultyService.findByID(id);
        FacultyDTO facultyDto = new FacultyDTO(faculty);
        return new ResponseEntity<>(facultyDto, HttpStatus.OK);
    }
}

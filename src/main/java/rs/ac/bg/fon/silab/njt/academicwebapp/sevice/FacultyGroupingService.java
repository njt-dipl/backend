/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.sevice;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.FacultyGrouping;
import rs.ac.bg.fon.silab.njt.academicwebapp.repository.FacultyGroupingRepository;

/**
 *
 * @author Anja
 */
@Service
public class FacultyGroupingService {
    @Autowired
    private FacultyGroupingRepository facultyGroupingRepository;

    public List<FacultyGrouping> findAll() {
        return facultyGroupingRepository.findAll();
    }
}

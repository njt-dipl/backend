package rs.ac.bg.fon.silab.njt.academicwebapp.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class AuthRequestDTO {

    @NotBlank(message = "Username may not be blank")
    private String username;

    @NotBlank(message = "Password may not be blank")
    private String password;
}

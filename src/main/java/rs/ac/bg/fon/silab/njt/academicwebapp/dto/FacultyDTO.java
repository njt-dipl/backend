/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.dto;

import java.util.List;
import javax.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.City;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.Faculty;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.FacultyGrouping;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.University;

/**
 *
 * @author Anja
 */
@Getter
@Setter
@NoArgsConstructor
public class FacultyDTO {

    private Long id;

//    @NotBlank(message = "name cannot be blank")
    private String name;

//    @NotBlank(message = "shortened name cannot be blank")
    private String shortenedName;

//    @NotBlank(message = "adress cannot be blank")
    private String address;

//    @NotBlank(message = "contact cannot be blank")
    private String contact;

//    @NotBlank(message = "university id cannot be blank")
    private Long universityId;

//    @NotBlank(message = "faculty grouping id cannot be blank")
    private Long facultyGroupingId;

//    @NotBlank(message = "postal code cannot be blank")
    private Long postalCode;
    
//    private List<StudyModuleDTO> studyModuleList;
//    
//    private List<SubjectDTO> subjectList;
//    
//    private List<StudyProgramDTO> studyProgramList;
//    
//    private List<SubjectConfigurationDTO> subjectConfigurationList;
//    
//    private List<SemesterConfigurationDTO> semesterConfigurationList;
    

    public FacultyDTO(Faculty faculty) {
        this.id = faculty.getIdFaculty();
        this.name = faculty.getName();
        this.shortenedName = faculty.getShortenedName();
        this.address = faculty.getAddress();
        this.contact = faculty.getContact();
//        this.universityId = faculty.getFacultyPK().getIdUniversity();
//        this.facultyGroupingId = faculty.getFacultyPK().getIdGrouping();
       
        if (faculty.getIdUniversity()== null) {
            this.universityId = null;
        } else {
            this.universityId = faculty.getIdUniversity().getIdUniversity();
        }      
        if (faculty.getIdGrouping() == null) {
            this.facultyGroupingId = null;
        } else {
            this.facultyGroupingId = faculty.getIdGrouping().getIdGrouping();
        }     
        if (faculty.getPostalCode() == null) {
            this.postalCode = null;
        } else {
            this.postalCode = faculty.getPostalCode().getPostalCode();
        }
    }

    public Faculty convertToEntity() {
        Faculty faculty = new Faculty();
        faculty.setName(this.name);
        faculty.setShortenedName(this.shortenedName);
        faculty.setAddress(this.address);
        faculty.setContact(this.contact);
//        faculty.setPostalCode(new City(postalCode));
//        faculty.setFacultyGrouping(new FacultyGrouping(facultyGroupingId));
//        faculty.setUniversity(new University(universityId));
//        faculty.setFacultyPK(new FacultyPK(universityId, facultyGroupingId, id));
        return faculty;
    }
    

}

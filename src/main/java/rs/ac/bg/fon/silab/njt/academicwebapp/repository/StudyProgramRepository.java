/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.StudyProgram;

/**
 *
 * @author Korisnik
 */
@Repository
public interface StudyProgramRepository extends JpaRepository<StudyProgram, Long> {

    @Query(value = "SELECT sp FROM StudyProgram sp WHERE sp.idFaculty.idFaculty=:facultyId")
    List<StudyProgram> findByFacultyId(@Param("facultyId") Long facultyId);

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Anja
 */
@Entity
@Table(name = "level_of_study")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LevelOfStudy.findAll", query = "SELECT l FROM LevelOfStudy l")
    , @NamedQuery(name = "LevelOfStudy.findById", query = "SELECT l FROM LevelOfStudy l WHERE l.id = :id")
    , @NamedQuery(name = "LevelOfStudy.findByName", query = "SELECT l FROM LevelOfStudy l WHERE l.name = :name")})
public class LevelOfStudy implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Size(max = 50)
    @Column(name = "name")
    private String name;
    @OneToMany(mappedBy = "levelOfStudy")
    private List<StudyProgram> studyProgramList;

    public LevelOfStudy() {
    }

    public LevelOfStudy(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public List<StudyProgram> getStudyProgramList() {
        return studyProgramList;
    }

    public void setStudyProgramList(List<StudyProgram> studyProgramList) {
        this.studyProgramList = studyProgramList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LevelOfStudy)) {
            return false;
        }
        LevelOfStudy other = (LevelOfStudy) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "rs.ac.bg.fon.silab.njt.academicwebapp.model.LevelOfStudy[ id=" + id + " ]";
    }
    
}

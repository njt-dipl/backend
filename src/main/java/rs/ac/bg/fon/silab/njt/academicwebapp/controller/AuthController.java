package rs.ac.bg.fon.silab.njt.academicwebapp.controller;

import java.util.Objects;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import rs.ac.bg.fon.silab.njt.academicwebapp.dto.AuthRequestDTO;
import rs.ac.bg.fon.silab.njt.academicwebapp.dto.AuthResponseDTO;
import rs.ac.bg.fon.silab.njt.academicwebapp.dto.UserProfileBasicDTO;
import rs.ac.bg.fon.silab.njt.academicwebapp.exception.AuthorizationException;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.UserProfile;
import rs.ac.bg.fon.silab.njt.academicwebapp.security.TokenUtils;
import rs.ac.bg.fon.silab.njt.academicwebapp.sevice.UserProfileService;

/**
 * REST controller for authentication
 */
@RestController
@RequiredArgsConstructor
@CrossOrigin("http://localhost:4200")
public class AuthController {

    @Autowired
    private final AuthenticationManager authenticationManager;
    @Autowired
    private final UserDetailsService userDetailsService;
    @Autowired
    private final UserProfileService userProfileService;
    @Autowired
    private final TokenUtils tokenUtils;

    /**
     * POST /api/auth Endpoint for user profiles authentication.
     *
     * @param authRequestDTO DTO containing login credentials
     * @return ResponseEntity containing the generated JSON Web Token
     */
    @PostMapping(path = "/authenticate")
    public ResponseEntity<AuthResponseDTO> authenticate(@Valid @RequestBody AuthRequestDTO authRequestDTO) throws Exception {

        System.out.println("usli smo u metodu authenticate");
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            authRequestDTO.getUsername(),
                            authRequestDTO.getPassword()
                    )
            );
            System.out.println("kraj metode   authenticationManager.authenticate ");

            final UserDetails userDetails = userDetailsService.loadUserByUsername(authRequestDTO.getUsername());
            System.out.println("user details : " + userDetails);

            final UserProfile userProfile = userProfileService.findByUsername(userDetails.getUsername());

            System.out.println("user profile : " + userProfile.getFirstname());
            UserProfileBasicDTO userProfileBasicDTO = new UserProfileBasicDTO(userProfile);

            if (!userProfile.getVerified()) {
                throw new AuthorizationException("User not verified.");
            }
            final String token = tokenUtils.generateToken(userDetails);
            return new ResponseEntity<>(new AuthResponseDTO(null, token), HttpStatus.OK);//dodati UserProfileBasicDTO umesto null

        } catch (BadCredentialsException ex) {
            throw new AuthorizationException("User credentials invalid.");
        }
    }

}

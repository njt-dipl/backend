/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.StudyModule;

/**
 *
 * @author Korisnik
 */
@Repository
public interface StudyModuleRepository extends JpaRepository<StudyModule, Long> {

    @Query(value = "SELECT sm FROM StudyModule sm WHERE sm.idStudyProgram.idStudyProgram=:idStudyProgram")
    List<StudyModule> findByStudyProgram(@Param("idStudyProgram") Long idStudyProgram);
    
    List<StudyModule> findAll();

    @Query(value = "SELECT sm FROM StudyModule sm WHERE sm.idStudyProgram.idStudyProgram=:idStudyProgram"
            + " AND sm.status=0")
    StudyModule findStudyProgramInModulByID(@Param("idStudyProgram") Long idStudyProgram);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.dto;

import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.University;

/**
 *
 * @author Anja
 */
@Getter
@Setter
@NoArgsConstructor
public class UniversityDTO {

    private Long idUniversity;

//    @NotBlank(message = "name cannot be blank")
    private String name;

//    @NotBlank(message = "postal code cannot be blank")
    private Long postalCode;
    
    private String imgPath;

//    @NotNull(message = "faculties cannot be null")
    private List<FacultyDTO> facultyList;
    
    

    public UniversityDTO(University university) {
        this.idUniversity = university.getIdUniversity();
        this.name = university.getName();
        this.imgPath = university.getImgPath();
        if (university.getPostalCode() == null) {
            this.postalCode = null;
        } else {
            this.postalCode = university.getPostalCode().getPostalCode();
        }
        this.facultyList = new ArrayList<>();
        university.getFacultyList().stream()
                .map(FacultyDTO::new)
                .forEach(this.facultyList::add);
    }

    public University convertToEntity() {
        University university = new University();
        university.setIdUniversity(this.idUniversity);
        university.setName(this.name);
        university.setImgPath(this.imgPath);
        university.setFacultyList(new ArrayList<>());
        this.facultyList.stream().map(FacultyDTO::convertToEntity)
                .forEach(university.getFacultyList()::add);

        return university;
    }
}

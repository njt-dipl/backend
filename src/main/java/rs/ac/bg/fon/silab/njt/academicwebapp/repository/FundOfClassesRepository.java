/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.FundOfClasses;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.SemesterConfiguration;

/**
 *
 * @author Anja
 */
@Repository
public interface FundOfClassesRepository extends JpaRepository<FundOfClasses, Long>{

    @Override
    public <S extends FundOfClasses> S save(S entity);
    
    
    @Query(value = "SELECT f FROM FundOfClasses f ORDER BY f.fundOfClassesPK.idTeachingForm asc")
    public List<FundOfClasses> findAll();

    @Query(value = "SELECT f FROM FundOfClasses f WHERE f.fundOfClassesPK.idSubject=:subjectId")
    public List<FundOfClasses> findAllForSubject(@Param("subjectId") Long subjectId);
}

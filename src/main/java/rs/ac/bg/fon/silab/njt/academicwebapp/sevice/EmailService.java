/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.sevice;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.Context;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.UserProfile;

/**
 *
 * @author Korisnik
 */
@Service
@RequiredArgsConstructor
public class EmailService {

    @Value("${spring.mail.username}")
    private String email;

    @Value("${templates.html.verification}")
    private String verificationTemplateName;

    @Value("success")
    private String successTemplate;

    private final JavaMailSender mailSender;
    private final ITemplateEngine springTemplateEngine;

    /**
     * Sends an email Throws an exception if there is an messagin error on the
     * SMTP
     *
     * @param userProfile
     * @param subject
     * @param content
     * @throws MessagingException
     */
    @Async
    public void sendMail(UserProfile userProfile, String subject, String content) throws MessagingException {
//        System.out.println("USLA U SEND MAIL");
        MimeMessage mailMessage = this.mailSender.createMimeMessage();
//        System.out.println("KREIRALA MIME MESSAGE");
        MimeMessageHelper messageHelper = new MimeMessageHelper(mailMessage, true); //"utf-8"
//        System.out.println("KREIRALA MIME MESSAGE HELPER");
        messageHelper.setTo(userProfile.getEmail());
//        System.out.println("USERM MAIL: " + userProfile.getEmail());
        messageHelper.setFrom(this.email);
//        System.out.println("SALJEM SA: " + this.email);
        messageHelper.setSubject(subject);
//        System.out.println("SUBJECT: " + subject);
        mailMessage.setContent(content, "text/html");
//        System.out.println("CONTENT: " + content);
        this.mailSender.send(mailMessage);
//        System.out.println("KRAJ METODE SEND MAILS");
    }

    /**
     * Sends a verification e-mail to the registered user. In case an messaging
     * error on the SMTP server occurs, a {@link MessagingException} is thrown.
     *
     * @param userProfile {@link UserProfile} instance as recipient
     * @throws MessagingException Exception thrown in case an error on the SMTP
     * server occurs
     */
    @Async
    public void sendVerificationEmail(UserProfile userProfile) throws MessagingException {
        this.sendMail(userProfile, "Please confirm your account", this.generateVerificationMail(userProfile));
    }

    /**
     * Generates verification e-mail content using a template, based on the
     * user's info
     *
     * @param userProfile
     * @return E-mail verification content
     */
    private String generateVerificationMail(UserProfile userProfile) {
        Map<String, Object> variables = new HashMap<>();
        variables.put("recipient", userProfile.getFirstname());
        variables.put("code", userProfile.getVerificationNumber());

        return this.springTemplateEngine
                .process(this.verificationTemplateName, new Context(Locale.getDefault(), variables));
    }

    public String successTemplate() {
        return this.springTemplateEngine.process(this.successTemplate, new Context(Locale.getDefault()));
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Anja
 */
@Entity
@Table(name = "fund_of_classes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FundOfClasses.findAll", query = "SELECT f FROM FundOfClasses f")
    , @NamedQuery(name = "FundOfClasses.findByIdSubject", query = "SELECT f FROM FundOfClasses f WHERE f.fundOfClassesPK.idSubject = :idSubject")
    , @NamedQuery(name = "FundOfClasses.findByIdTeachingForm", query = "SELECT f FROM FundOfClasses f WHERE f.fundOfClassesPK.idTeachingForm = :idTeachingForm")
    , @NamedQuery(name = "FundOfClasses.findByNumberOfClasses", query = "SELECT f FROM FundOfClasses f WHERE f.numberOfClasses = :numberOfClasses")})
public class FundOfClasses implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FundOfClassesPK fundOfClassesPK;
    @Column(name = "number_of_classes")
    private Integer numberOfClasses;
    @JoinColumn(name = "id_subject", referencedColumnName = "id_subject", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Subject subject;
    @JoinColumn(name = "id_teaching_form", referencedColumnName = "id_teaching_form", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private TeachingForm teachingForm;

    public FundOfClasses() {
    }

    public FundOfClasses(FundOfClassesPK fundOfClassesPK) {
        this.fundOfClassesPK = fundOfClassesPK;
    }

    public FundOfClasses(long idSubject, long idTeachingForm) {
        this.fundOfClassesPK = new FundOfClassesPK(idSubject, idTeachingForm);
    }

    public FundOfClassesPK getFundOfClassesPK() {
        return fundOfClassesPK;
    }

    public void setFundOfClassesPK(FundOfClassesPK fundOfClassesPK) {
        this.fundOfClassesPK = fundOfClassesPK;
    }

    public Integer getNumberOfClasses() {
        return numberOfClasses;
    }

    public void setNumberOfClasses(Integer numberOfClasses) {
        this.numberOfClasses = numberOfClasses;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public TeachingForm getTeachingForm() {
        return teachingForm;
    }

    public void setTeachingForm(TeachingForm teachingForm) {
        this.teachingForm = teachingForm;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fundOfClassesPK != null ? fundOfClassesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FundOfClasses)) {
            return false;
        }
        FundOfClasses other = (FundOfClasses) object;
        if ((this.fundOfClassesPK == null && other.fundOfClassesPK != null) || (this.fundOfClassesPK != null && !this.fundOfClassesPK.equals(other.fundOfClassesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "rs.ac.bg.fon.silab.njt.academicwebapp.model.FundOfClasses[ fundOfClassesPK=" + fundOfClassesPK + " ]";
    }
    
}

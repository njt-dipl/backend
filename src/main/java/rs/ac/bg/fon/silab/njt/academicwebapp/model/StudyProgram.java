/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Anja
 */
@Entity
@Table(name = "study_program")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StudyProgram.findAll", query = "SELECT s FROM StudyProgram s")
    , @NamedQuery(name = "StudyProgram.findByIdStudyProgram", query = "SELECT s FROM StudyProgram s WHERE s.idStudyProgram = :idStudyProgram")
    , @NamedQuery(name = "StudyProgram.findByName", query = "SELECT s FROM StudyProgram s WHERE s.name = :name")
    , @NamedQuery(name = "StudyProgram.findByShortenedName", query = "SELECT s FROM StudyProgram s WHERE s.shortenedName = :shortenedName")
    , @NamedQuery(name = "StudyProgram.findByNumberOfSemesters", query = "SELECT s FROM StudyProgram s WHERE s.numberOfSemesters = :numberOfSemesters")})
public class StudyProgram implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_study_program")
    private Long idStudyProgram;
    @Size(max = 50)
    @Column(name = "name")
    private String name;
    @Size(max = 50)
    @Column(name = "shortened_name")
    private String shortenedName;
    @Column(name = "number_of_semesters")
    private Integer numberOfSemesters;
    @OneToMany(mappedBy = "idStudyProgram")
    private List<StudyModule> studyModuleList;
    @JoinColumn(name = "id_faculty", referencedColumnName = "id_faculty")
    @ManyToOne
    private Faculty idFaculty;
    @JoinColumn(name = "level_of_study", referencedColumnName = "id")
    @ManyToOne
    private LevelOfStudy levelOfStudy;
    @OneToMany(mappedBy = "idStudyProgram")
    private List<SemesterConfiguration> semesterConfigurationList;

    public StudyProgram() {
    }

    public StudyProgram(Long idStudyProgram) {
        this.idStudyProgram = idStudyProgram;
    }

    public Long getIdStudyProgram() {
        return idStudyProgram;
    }

    public void setIdStudyProgram(Long idStudyProgram) {
        this.idStudyProgram = idStudyProgram;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortenedName() {
        return shortenedName;
    }

    public void setShortenedName(String shortenedName) {
        this.shortenedName = shortenedName;
    }

    public Integer getNumberOfSemesters() {
        return numberOfSemesters;
    }

    public void setNumberOfSemesters(Integer numberOfSemesters) {
        this.numberOfSemesters = numberOfSemesters;
    }

    @XmlTransient
    public List<StudyModule> getStudyModuleList() {
        return studyModuleList;
    }

    public void setStudyModuleList(List<StudyModule> studyModuleList) {
        this.studyModuleList = studyModuleList;
    }

    public Faculty getIdFaculty() {
        return idFaculty;
    }

    public void setIdFaculty(Faculty idFaculty) {
        this.idFaculty = idFaculty;
    }

    public LevelOfStudy getLevelOfStudy() {
        return levelOfStudy;
    }

    public void setLevelOfStudy(LevelOfStudy levelOfStudy) {
        this.levelOfStudy = levelOfStudy;
    }

    @XmlTransient
    public List<SemesterConfiguration> getSemesterConfigurationList() {
        return semesterConfigurationList;
    }

    public void setSemesterConfigurationList(List<SemesterConfiguration> semesterConfigurationList) {
        this.semesterConfigurationList = semesterConfigurationList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idStudyProgram != null ? idStudyProgram.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StudyProgram)) {
            return false;
        }
        StudyProgram other = (StudyProgram) object;
        if ((this.idStudyProgram == null && other.idStudyProgram != null) || (this.idStudyProgram != null && !this.idStudyProgram.equals(other.idStudyProgram))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "rs.ac.bg.fon.silab.njt.academicwebapp.model.StudyProgram[ idStudyProgram=" + idStudyProgram + " ]";
    }
    
}

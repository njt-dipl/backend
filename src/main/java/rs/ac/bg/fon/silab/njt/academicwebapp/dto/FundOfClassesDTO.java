/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.FundOfClasses;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.FundOfClassesPK;


/**
 *
 * @author Anja
 */
@Getter
@Setter
@NoArgsConstructor
public class FundOfClassesDTO {
    
    private Long subjectId;
    private Long teachingFormId;
    private int numberOfClasses;
    private String teachingFormName;
    
    public FundOfClassesDTO(FundOfClasses foc) {
        this.subjectId = foc.getSubject().getIdSubject();
        this.teachingFormId = foc.getTeachingForm().getIdTeachingForm();
        this.numberOfClasses = foc.getNumberOfClasses();
        this.teachingFormName = foc.getTeachingForm().getName();
    }
    
    public FundOfClasses convertToEntity() {
        FundOfClasses foc = new FundOfClasses();
        foc.setNumberOfClasses(this.numberOfClasses);
//        foc.setTeachingForm(new TeachingForm(teachingFormId));
//        foc.setSubject(new Subject(subjectId));
        foc.setFundOfClassesPK(new FundOfClassesPK(subjectId, teachingFormId));
        return foc;
    }
    
    @Override
    public String toString() {
        return "FundOfClassesDTO{" + "subjectId=" + subjectId + ", teachingFormId=" + teachingFormId + ", numberOfClasses=" + numberOfClasses + '}';
    }
    
}

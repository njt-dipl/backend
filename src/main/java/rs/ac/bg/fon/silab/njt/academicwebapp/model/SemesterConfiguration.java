/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Anja
 */
@Entity
@Table(name = "semester_configuration")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SemesterConfiguration.findAll", query = "SELECT s FROM SemesterConfiguration s")
    , @NamedQuery(name = "SemesterConfiguration.findByIdSemesterConfiguration", query = "SELECT s FROM SemesterConfiguration s WHERE s.idSemesterConfiguration = :idSemesterConfiguration")
    , @NamedQuery(name = "SemesterConfiguration.findBySemester", query = "SELECT s FROM SemesterConfiguration s WHERE s.semester = :semester")
    , @NamedQuery(name = "SemesterConfiguration.findByPosition", query = "SELECT s FROM SemesterConfiguration s WHERE s.position = :position")
    , @NamedQuery(name = "SemesterConfiguration.findByEspb", query = "SELECT s FROM SemesterConfiguration s WHERE s.espb = :espb")
    , @NamedQuery(name = "SemesterConfiguration.findByMandatory", query = "SELECT s FROM SemesterConfiguration s WHERE s.mandatory = :mandatory")
    , @NamedQuery(name = "SemesterConfiguration.findByStatus", query = "SELECT s FROM SemesterConfiguration s WHERE s.status = :status")})
public class SemesterConfiguration implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_semester_configuration")
    private Long idSemesterConfiguration;
    @Column(name = "semester")
    private Integer semester;
    @Column(name = "position")
    private Integer position;
    @Column(name = "espb")
    private Integer espb;
    @Size(max = 50)
    @Column(name = "mandatory")
    private String mandatory;
    @Column(name = "status")
    private Boolean status;
    @OneToMany(mappedBy = "idSemesterConfiguration")
    private List<SubjectConfiguration> subjectConfigurationList;
    @JoinColumn(name = "id_study_module", referencedColumnName = "id_study_module")
    @ManyToOne
    private StudyModule idStudyModule;
    @JoinColumn(name = "id_study_program", referencedColumnName = "id_study_program")
    @ManyToOne
    private StudyProgram idStudyProgram;
    @JoinColumn(name = "id_subject_type", referencedColumnName = "id_subject_type")
    @ManyToOne
    private SubjectType idSubjectType;

    public SemesterConfiguration() {
    }

    public SemesterConfiguration(Long idSemesterConfiguration) {
        this.idSemesterConfiguration = idSemesterConfiguration;
    }

    public Long getIdSemesterConfiguration() {
        return idSemesterConfiguration;
    }

    public void setIdSemesterConfiguration(Long idSemesterConfiguration) {
        this.idSemesterConfiguration = idSemesterConfiguration;
    }

    public Integer getSemester() {
        return semester;
    }

    public void setSemester(Integer semester) {
        this.semester = semester;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Integer getEspb() {
        return espb;
    }

    public void setEspb(Integer espb) {
        this.espb = espb;
    }

    public String getMandatory() {
        return mandatory;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @XmlTransient
    public List<SubjectConfiguration> getSubjectConfigurationList() {
        return subjectConfigurationList;
    }

    public void setSubjectConfigurationList(List<SubjectConfiguration> subjectConfigurationList) {
        this.subjectConfigurationList = subjectConfigurationList;
    }

    public StudyModule getIdStudyModule() {
        return idStudyModule;
    }

    public void setIdStudyModule(StudyModule idStudyModule) {
        this.idStudyModule = idStudyModule;
    }

    public StudyProgram getIdStudyProgram() {
        return idStudyProgram;
    }

    public void setIdStudyProgram(StudyProgram idStudyProgram) {
        this.idStudyProgram = idStudyProgram;
    }

    public SubjectType getIdSubjectType() {
        return idSubjectType;
    }

    public void setIdSubjectType(SubjectType idSubjectType) {
        this.idSubjectType = idSubjectType;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSemesterConfiguration != null ? idSemesterConfiguration.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SemesterConfiguration)) {
            return false;
        }
        SemesterConfiguration other = (SemesterConfiguration) object;
        if ((this.idSemesterConfiguration == null && other.idSemesterConfiguration != null) || (this.idSemesterConfiguration != null && !this.idSemesterConfiguration.equals(other.idSemesterConfiguration))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "rs.ac.bg.fon.silab.njt.academicwebapp.model.SemesterConfiguration[ idSemesterConfiguration=" + idSemesterConfiguration + " ]";
    }
    
}

package rs.ac.bg.fon.silab.njt.academicwebapp.security;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.UserProfile;
import rs.ac.bg.fon.silab.njt.academicwebapp.repository.UserProfileRepository;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Implementation of {@link UserDetailsService}.
 */
@Service("userDetailsService")
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private final UserProfileRepository userProfileRepository;

    /**
     * Generates a {@link UserDetails} instance for given username In case an
     * user with given username is not found, {@link UsernameNotFoundException}
     * is thrown.
     *
     * @param username User profile's username
     * @return {@link UserDetails} instance
     * @throws {@link UsernameNotFoundException} exception in case an user with
     * given username is not found
     */
//    static final List<SpringSecurityUserProfile> inMemoryUserList = new ArrayList<>();
//
//    static {
//        inMemoryUserList.add(new SpringSecurityUserProfile(1L, "in28minutes",
//                "$2a$10$3zHzb.Npv1hfZbLEU5qsdOju/tk2je6W6PnNnY.c1ujWPcZh4PL6e", "ROLE_USER_2"));
//        inMemoryUserList.add(new SpringSecurityUserProfile(2L, "ranga",
//                "$2a$10$IetbreuU5KihCkDB6/r1DOJO0VyU9lSiBcrMDT.biU7FOt2oqZDPm", "ROLE_USER_2"));
//
//        //$2a$10$IetbreuU5KihCkDB6/r1DOJO0VyU9lSiBcrMDT.biU7FOt2oqZDPm
//    }
//
//    @Override
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        Optional<SpringSecurityUserProfile> findFirst = inMemoryUserList.stream()
//                .filter(user -> user.getUsername().equals(username)).findFirst();
//
//        if (!findFirst.isPresent()) {
//            throw new UsernameNotFoundException(String.format("USER_NOT_FOUND '%s'.", username));
//        }
//
//        return findFirst.get();
//    }
    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        UserProfile userProfile = userProfileRepository.findByUsernameIgnoreCase(username)
                .orElseThrow(() -> new UsernameNotFoundException(String.format("No user found with username '%s'.", username)));

        return UserDetailsFactory.create(userProfile);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.controller;

import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rs.ac.bg.fon.silab.njt.academicwebapp.dto.FundOfClassesDTO;
import rs.ac.bg.fon.silab.njt.academicwebapp.dto.SubjectDTO;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.Faculty;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.FundOfClasses;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.Subject;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.SubjectType;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.TeachingForm;
import rs.ac.bg.fon.silab.njt.academicwebapp.sevice.FundOfClassesService;
import rs.ac.bg.fon.silab.njt.academicwebapp.sevice.SubjectService;
import rs.ac.bg.fon.silab.njt.academicwebapp.sevice.TeachingFormService;

/**
 *
 * @author Anja
 */
@RestController
@RequestMapping("/fundOfClasses")
@CrossOrigin("http://localhost:4200")
public class FundOfClassesController {
    
    @Autowired
    private FundOfClassesService fundOfClassesService;
    @Autowired
    private SubjectService subjectService;
    @Autowired
    private TeachingFormService teachingFormService;
    
    @PostMapping(path = "/save")
    public ResponseEntity<FundOfClassesDTO> createFundOfClasses(
            @Valid @RequestBody FundOfClassesDTO dto) {
        System.out.println("********************************************************DTO: "+dto);
//        System.out.println("U METODI SAM MOJOJ: "+dto.getNumberOfClasses());
        FundOfClasses foc = dto.convertToEntity();
        Subject subject = subjectService.findByID(dto.getSubjectId());
        foc.setSubject(subject);
        TeachingForm tf = teachingFormService.fidByID(dto.getTeachingFormId());
        foc.setTeachingForm(tf);
        fundOfClassesService.create(foc);
        return new ResponseEntity<>(new FundOfClassesDTO(foc), HttpStatus.OK);
    }

    @GetMapping("/getAll")
    public ResponseEntity<List<FundOfClassesDTO>> getAll() {
        List<FundOfClassesDTO> funds = new ArrayList<>();
        fundOfClassesService.findAll().stream()
                .map(FundOfClassesDTO::new)
                .forEachOrdered(funds::add);
        
        return new ResponseEntity<>(funds, HttpStatus.OK);
    }
    
    ///getAll/${subjectId}
    
    @GetMapping("/getAll/{subjectId}")
    public ResponseEntity<List<FundOfClassesDTO>> getAllForSubject(
            @PathVariable("subjectId") Long id) {
        List<FundOfClassesDTO> funds = new ArrayList<>();
       fundOfClassesService.findAllForSubject(id).stream()
                .map(FundOfClassesDTO::new)
                .forEachOrdered(funds::add);

        for (FundOfClassesDTO fund : funds) {
             System.out.println("FONDOVI"+fund);
        }
       
       
        return new ResponseEntity<>(funds, HttpStatus.OK);
    }
}

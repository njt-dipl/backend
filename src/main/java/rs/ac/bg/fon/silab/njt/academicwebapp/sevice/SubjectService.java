/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.sevice;

import java.util.List;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.bg.fon.silab.njt.academicwebapp.exception.UnprocessableEntryException;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.Subject;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.SubjectType;
import rs.ac.bg.fon.silab.njt.academicwebapp.repository.SubjectRepository;

/**
 *
 * @author Anja
 */
@Service
public class SubjectService {

    @Autowired
    private SubjectRepository subjectRepository;
    @Autowired
    private SubjectTypeService subjectTypeService;

    public List<Subject> findAll() {
        return subjectRepository.findAll();
    }

    public Subject findByID(Long id) {
        return subjectRepository.findById(id).orElseThrow(UnprocessableEntryException::new);
    }

    public List<Subject> findAllForFaculty(Long id) {
        return subjectRepository.findAllFromFaculty(id);
    }

    public Subject create(Subject subject) {
        return subjectRepository.save(subject);
    }
    
//    public Long create(Subject s){
//        return  subjectRepository.save(s)
//    }

    public Subject update(Subject updatedSubject) throws NotFoundException {
        final Subject subject = subjectRepository.findById(updatedSubject.getIdSubject()).
                orElseThrow(() -> new NotFoundException("Subject not found."));
        subject.setName(updatedSubject.getName());
        subject.setShortenedName(updatedSubject.getShortenedName());
        SubjectType st = subjectTypeService.findByID(updatedSubject.getIdSubjectType().getIdSubjectType());
        subject.setIdSubjectType(st);
        subject.setEspb(updatedSubject.getEspb());
        return this.subjectRepository.save(subject);
    }

    public void delete(Long idSubject) {
        subjectRepository.deleteById(idSubject);
    }

    public List<Subject> findAllForSubjectType(Long idSubjectType) {
        return subjectRepository.findAllForSubjectType(idSubjectType);
    }

}

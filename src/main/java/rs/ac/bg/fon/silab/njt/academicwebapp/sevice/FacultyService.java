/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.sevice;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.bg.fon.silab.njt.academicwebapp.exception.UnprocessableEntryException;
import rs.ac.bg.fon.silab.njt.academicwebapp.model.Faculty;
import rs.ac.bg.fon.silab.njt.academicwebapp.repository.FacultyRepository;

/**
 *
 * @author Anja
 */
@Service
public class FacultyService {
    
    @Autowired
    private FacultyRepository facultyRepository;
    
    public List<Faculty> findAll() {
        return facultyRepository.findAll();
    }

    public Faculty findByID(Long id) {
        return facultyRepository.findById(id).orElseThrow(UnprocessableEntryException::new);
    }
}

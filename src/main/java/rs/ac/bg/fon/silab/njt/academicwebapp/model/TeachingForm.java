/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Anja
 */
@Entity
@Table(name = "teaching_form")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TeachingForm.findAll", query = "SELECT t FROM TeachingForm t")
    , @NamedQuery(name = "TeachingForm.findByIdTeachingForm", query = "SELECT t FROM TeachingForm t WHERE t.idTeachingForm = :idTeachingForm")
    , @NamedQuery(name = "TeachingForm.findByName", query = "SELECT t FROM TeachingForm t WHERE t.name = :name")
    , @NamedQuery(name = "TeachingForm.findByShortenedName", query = "SELECT t FROM TeachingForm t WHERE t.shortenedName = :shortenedName")})
public class TeachingForm implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_teaching_form")
    private Long idTeachingForm;
    @Size(max = 50)
    @Column(name = "name")
    private String name;
    @Size(max = 10)
    @Column(name = "shortened_name")
    private String shortenedName;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "teachingForm")
    private List<FundOfClasses> fundOfClassesList;

    public TeachingForm() {
    }

    public TeachingForm(Long idTeachingForm) {
        this.idTeachingForm = idTeachingForm;
    }

    public Long getIdTeachingForm() {
        return idTeachingForm;
    }

    public void setIdTeachingForm(Long idTeachingForm) {
        this.idTeachingForm = idTeachingForm;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortenedName() {
        return shortenedName;
    }

    public void setShortenedName(String shortenedName) {
        this.shortenedName = shortenedName;
    }

    @XmlTransient
    public List<FundOfClasses> getFundOfClassesList() {
        return fundOfClassesList;
    }

    public void setFundOfClassesList(List<FundOfClasses> fundOfClassesList) {
        this.fundOfClassesList = fundOfClassesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTeachingForm != null ? idTeachingForm.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TeachingForm)) {
            return false;
        }
        TeachingForm other = (TeachingForm) object;
        if ((this.idTeachingForm == null && other.idTeachingForm != null) || (this.idTeachingForm != null && !this.idTeachingForm.equals(other.idTeachingForm))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "rs.ac.bg.fon.silab.njt.academicwebapp.model.TeachingForm[ idTeachingForm=" + idTeachingForm + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.njt.academicwebapp.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rs.ac.bg.fon.silab.njt.academicwebapp.dto.FacultyGroupingDTO;
import rs.ac.bg.fon.silab.njt.academicwebapp.sevice.FacultyGroupingService;

/**
 *
 * @author Anja
 */
@RestController
@RequestMapping("/facultyGrouping")
@CrossOrigin("http://localhost:4200")
public class FacultyGroupingController {
    
    @Autowired
    private FacultyGroupingService facultyGroupingService;
    
    @GetMapping("/getAll")
    public ResponseEntity<List<FacultyGroupingDTO>> getAll() {
        List<FacultyGroupingDTO> groupings = new ArrayList<>();
        facultyGroupingService.findAll().stream()
                .map(FacultyGroupingDTO::new)
            .forEachOrdered(groupings::add);

        return new ResponseEntity<>(groupings, HttpStatus.OK);
    }
}

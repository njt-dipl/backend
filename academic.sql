/*
SQLyog Community v13.1.7 (64 bit)
MySQL - 10.4.17-MariaDB : Database - academic
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`academic` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `academic`;

/*Table structure for table `city` */

DROP TABLE IF EXISTS `city`;

CREATE TABLE `city` (
  `postal_code` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`postal_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `city` */

insert  into `city`(`postal_code`,`name`,`country`) values 
(11000,'Beograd','Srbija'),
(18000,'Niš','Srbija'),
(21000,'Novi Sad','Srbija');

/*Table structure for table `faculty` */

DROP TABLE IF EXISTS `faculty`;

CREATE TABLE `faculty` (
  `id_faculty` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `shortened_name` varchar(50) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `postal_code` bigint(20) DEFAULT NULL,
  `id_university` bigint(20) DEFAULT NULL,
  `id_grouping` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_faculty`),
  KEY `fk_faculty_university` (`id_university`),
  KEY `fk_faculty_grouping` (`id_grouping`),
  KEY `fk_faculty_city` (`postal_code`),
  CONSTRAINT `fk_faculty_city` FOREIGN KEY (`postal_code`) REFERENCES `city` (`postal_code`),
  CONSTRAINT `fk_faculty_grouping` FOREIGN KEY (`id_grouping`) REFERENCES `faculty_grouping` (`id_grouping`),
  CONSTRAINT `fk_faculty_university` FOREIGN KEY (`id_university`) REFERENCES `university` (`id_university`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Data for the table `faculty` */

insert  into `faculty`(`id_faculty`,`name`,`shortened_name`,`address`,`contact`,`postal_code`,`id_university`,`id_grouping`) values 
(2,'Fakultet organizacionih nauka','FON','Jove Ilića 154','011-3950-800',11000,1,4),
(3,'Medicinski fakultet','MF','Bulevar dr Zorana Đinđića 81','+381-18-42-26-644',18000,3,2),
(4,'Fakultet Tehničkih nauka','FTN','Trg Dositeja Obradovića 6','021-450-810',21000,2,4);

/*Table structure for table `faculty_grouping` */

DROP TABLE IF EXISTS `faculty_grouping`;

CREATE TABLE `faculty_grouping` (
  `id_grouping` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_grouping`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `faculty_grouping` */

insert  into `faculty_grouping`(`id_grouping`,`name`) values 
(1,'Društveno-humanističke nauke'),
(2,'Medicinske Nauke'),
(3,'Prirodno-matematičke nauke'),
(4,'Tehničko-tehnološke nauke');

/*Table structure for table `fund_of_classes` */

DROP TABLE IF EXISTS `fund_of_classes`;

CREATE TABLE `fund_of_classes` (
  `id_subject` bigint(20) NOT NULL,
  `id_teaching_form` bigint(10) NOT NULL,
  `number_of_classes` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_subject`,`id_teaching_form`),
  KEY `fk_fund_subject` (`id_subject`),
  KEY `fk_fund_teaching_form` (`id_teaching_form`),
  CONSTRAINT `fk_fund_subject` FOREIGN KEY (`id_subject`) REFERENCES `subject` (`id_subject`),
  CONSTRAINT `fk_fund_teaching_form` FOREIGN KEY (`id_teaching_form`) REFERENCES `teaching_form` (`id_teaching_form`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `fund_of_classes` */

insert  into `fund_of_classes`(`id_subject`,`id_teaching_form`,`number_of_classes`) values 
(11,1,2),
(11,2,1),
(11,3,0),
(12,1,0),
(12,2,2),
(12,3,1),
(13,1,2),
(13,2,2),
(13,3,0),
(32,1,2),
(32,2,2),
(32,3,0),
(32,4,0),
(44,1,2),
(44,2,2),
(44,3,2);

/*Table structure for table `level_of_study` */

DROP TABLE IF EXISTS `level_of_study`;

CREATE TABLE `level_of_study` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

/*Data for the table `level_of_study` */

insert  into `level_of_study`(`id`,`name`) values 
(1,'Osnovne studije'),
(2,'Master studije'),
(3,'Doktorske studije');

/*Table structure for table `semester_configuration` */

DROP TABLE IF EXISTS `semester_configuration`;

CREATE TABLE `semester_configuration` (
  `id_semester_configuration` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_study_program` bigint(20) DEFAULT NULL,
  `id_study_module` bigint(20) DEFAULT NULL,
  `semester` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `espb` int(11) DEFAULT NULL,
  `mandatory` varchar(50) DEFAULT NULL,
  `id_subject_type` bigint(10) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_semester_configuration`),
  KEY `fk_semconf_subject_type` (`id_subject_type`),
  KEY `fk_semconf_study_program` (`id_study_program`),
  KEY `fk_semconf_study_module` (`id_study_module`),
  CONSTRAINT `fk_semconf_study_module` FOREIGN KEY (`id_study_module`) REFERENCES `study_module` (`id_study_module`),
  CONSTRAINT `fk_semconf_study_program` FOREIGN KEY (`id_study_program`) REFERENCES `study_program` (`id_study_program`),
  CONSTRAINT `fk_semconf_subject_type` FOREIGN KEY (`id_subject_type`) REFERENCES `subject_type` (`id_subject_type`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8mb4;

/*Data for the table `semester_configuration` */

insert  into `semester_configuration`(`id_semester_configuration`,`id_study_program`,`id_study_module`,`semester`,`position`,`espb`,`mandatory`,`id_subject_type`,`status`) values 
(87,1,1,1,1,6,'Obavezan na nivou studijskog programa',1,0),
(88,1,3,1,1,6,'Obavezan na nivou studijskog programa',1,0),
(89,1,4,1,1,6,'Obavezan na nivou studijskog programa',1,0);

/*Table structure for table `study_module` */

DROP TABLE IF EXISTS `study_module`;

CREATE TABLE `study_module` (
  `id_study_module` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `shortened_name` varchar(50) DEFAULT NULL,
  `start_year` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `id_faculty` bigint(20) DEFAULT NULL,
  `id_study_program` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_study_module`),
  KEY `fk_module_faculty` (`id_faculty`),
  KEY `fk_module_study_program` (`id_study_program`),
  CONSTRAINT `fk_module_faculty` FOREIGN KEY (`id_faculty`) REFERENCES `faculty` (`id_faculty`),
  CONSTRAINT `fk_module_study_program` FOREIGN KEY (`id_study_program`) REFERENCES `study_program` (`id_study_program`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

/*Data for the table `study_module` */

insert  into `study_module`(`id_study_module`,`name`,`shortened_name`,`start_year`,`status`,`id_faculty`,`id_study_program`) values 
(1,'Menadzment kvaliteta i standardizacija','mks',2,1,2,1),
(2,'Softverski inzenjering','si',2,1,2,2),
(3,'Operacioni menadžment','om',2,1,2,1),
(4,'Menadžment','sp-men',1,0,2,1),
(5,'Informacioni sistemi i tehnologije','sp-isit',1,0,2,2);

/*Table structure for table `study_program` */

DROP TABLE IF EXISTS `study_program`;

CREATE TABLE `study_program` (
  `id_study_program` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `shortened_name` varchar(50) DEFAULT NULL,
  `number_of_semesters` int(11) DEFAULT NULL,
  `level_of_study` bigint(20) DEFAULT NULL,
  `id_faculty` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_study_program`),
  KEY `fk_sp_level_od_study` (`level_of_study`),
  KEY `id_sp_faculty` (`id_faculty`),
  CONSTRAINT `fk_sp_faculty` FOREIGN KEY (`id_faculty`) REFERENCES `faculty` (`id_faculty`),
  CONSTRAINT `fk_sp_level_od_study` FOREIGN KEY (`level_of_study`) REFERENCES `level_of_study` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `study_program` */

insert  into `study_program`(`id_study_program`,`name`,`shortened_name`,`number_of_semesters`,`level_of_study`,`id_faculty`) values 
(1,'Menadžment','men',8,1,2),
(2,'Informacioni sistemi i tehnologije','isit',8,1,2);

/*Table structure for table `subject` */

DROP TABLE IF EXISTS `subject`;

CREATE TABLE `subject` (
  `id_subject` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `shortened_name` varchar(50) DEFAULT NULL,
  `espb` int(11) DEFAULT NULL,
  `id_faculty` bigint(20) DEFAULT NULL,
  `id_subject_type` bigint(10) DEFAULT NULL,
  PRIMARY KEY (`id_subject`),
  KEY `fk_subject_faculty` (`id_faculty`),
  KEY `fk_subject_subject_type` (`id_subject_type`),
  CONSTRAINT `fk_subject_faculty` FOREIGN KEY (`id_faculty`) REFERENCES `faculty` (`id_faculty`),
  CONSTRAINT `fk_subject_subject_type` FOREIGN KEY (`id_subject_type`) REFERENCES `subject_type` (`id_subject_type`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4;

/*Data for the table `subject` */

insert  into `subject`(`id_subject`,`name`,`shortened_name`,`espb`,`id_faculty`,`id_subject_type`) values 
(11,'Operaciona istraživanja 1','OI1',6,2,1),
(12,'Proizvodni sistemi','PS',4,2,2),
(13,'Osnove organizacije','OO',5,2,2),
(32,'Operaciona istraživanja 2','OI2',6,2,3),
(44,'Menadzment inovacija','MI',5,2,2);

/*Table structure for table `subject_configuration` */

DROP TABLE IF EXISTS `subject_configuration`;

CREATE TABLE `subject_configuration` (
  `id_subject_configuration` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_subject` bigint(20) DEFAULT NULL,
  `id_semester_configuration` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_subject_configuration`),
  KEY `fk_sabc_subject` (`id_subject`),
  KEY `fk_sabc_semester_configuration` (`id_semester_configuration`),
  CONSTRAINT `fk_sabc_semester_configuration` FOREIGN KEY (`id_semester_configuration`) REFERENCES `semester_configuration` (`id_semester_configuration`),
  CONSTRAINT `fk_sabc_subject` FOREIGN KEY (`id_subject`) REFERENCES `subject` (`id_subject`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

/*Data for the table `subject_configuration` */

insert  into `subject_configuration`(`id_subject_configuration`,`id_subject`,`id_semester_configuration`) values 
(1,11,87),
(2,11,88),
(3,11,89);

/*Table structure for table `subject_type` */

DROP TABLE IF EXISTS `subject_type`;

CREATE TABLE `subject_type` (
  `id_subject_type` bigint(10) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(255) DEFAULT NULL,
  `shortened_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_subject_type`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Data for the table `subject_type` */

insert  into `subject_type`(`id_subject_type`,`full_name`,`shortened_name`) values 
(1,'Akademsko-opšteobrazovni predmet','AO'),
(2,'Teorijsko-metodološki predmet','TM'),
(3,'Naučno-stručni predmet','NS'),
(4,'Stručno-aplikativni predmet','SA');

/*Table structure for table `teaching_form` */

DROP TABLE IF EXISTS `teaching_form`;

CREATE TABLE `teaching_form` (
  `id_teaching_form` bigint(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `shortened_name` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_teaching_form`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Data for the table `teaching_form` */

insert  into `teaching_form`(`id_teaching_form`,`name`,`shortened_name`) values 
(1,'Predavanja','p'),
(2,'Vežbe','v'),
(3,'Praktične/laboratorijske vežbe','lab'),
(4,'Drugi oblici nastave','drugo');

/*Table structure for table `university` */

DROP TABLE IF EXISTS `university`;

CREATE TABLE `university` (
  `id_university` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postal_code` bigint(20) DEFAULT NULL,
  `img_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_university`),
  KEY `fk_university_city` (`postal_code`),
  CONSTRAINT `fk_university_city` FOREIGN KEY (`postal_code`) REFERENCES `city` (`postal_code`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `university` */

insert  into `university`(`id_university`,`name`,`postal_code`,`img_path`) values 
(1,'Univerzitet u Beogradu',11000,'1.png'),
(2,'Univerzitet u Novom Sadu',21000,'2.png'),
(3,'Univerzitet u Nišu',18000,'3.png'),
(4,'Univerzitet Singidunum',11000,'4.png');

/*Table structure for table `user_profile` */

DROP TABLE IF EXISTS `user_profile`;

CREATE TABLE `user_profile` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verified` tinyint(1) DEFAULT NULL,
  `verification_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `creation_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `user_profile` */

insert  into `user_profile`(`id`,`firstname`,`lastname`,`username`,`password`,`email`,`verified`,`verification_number`,`creation_date`) values 
(46,'admin','admin','admin','$2a$10$gDz3ao1mWaZuoYGDFx13SOvt99qkZCwJ6GjCWFOZDnzU4GigS.uZe','lazarpopadic09@gmail.com',1,NULL,'2021-06-19');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
